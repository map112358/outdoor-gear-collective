<?php

function printUserCard($usersName) {
  $header = "background: url('img/".$usersName['image_header']['name']."'), url('img/default_user_header.jpg'); background-repeat: no-repeat; background-size:cover;";
?>
<div class="col-xs-12 col-sm-6 col-md-4" onclick="location.href='user.php?id=<?=$usersName['id']?>'">
  <div class="card hovercard pointer">
      <div class="cardheader" style="<?=$header?>">
  
      </div>
      <div class="avatar">
          <img alt="" src="img/<?=$usersName['image']['name']?>" onerror="if (this.src != 'img/default_user.png') this.src = 'img/default_user.png';">
      </div>
      <div class="info">
          <div class="title">
              <a ><?=$usersName['name_first']?> <?=$usersName['name_last']?></a>
          </div>
          <!--  <div class='desc'><?=$usersName['name_trail']?></div> -->
          
      </div>
      <div class="bottom">
      </div>
  </div>
</div>
<?php
}

  
function printGearSearch() {
?>
  <div clsss="row">
    <div class="margin-sm-bottom">
      <div class="form-group">
    		<input id="search" type="text" placeholder="Search" class="form-control" name="search" >
    	</div>
    </div>
  </div>
<?php
}

function printCategorySelector() {
  global $db;
  $category = ModuleCategory::select($db->filter(array()));
  $out = "<div class='dropdown margin-sm-bottom'>
  <button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>Select Category
  <span class='caret'></span></button><ul class='dropdown-menu multi-level'>";
  foreach($category AS $c){
    $out .= "<li>";
    $out .= "<a href='module.php?module_category_id={$c['id']}'>".$c['name']."</a>";
    $out .= "</li>";
  }
  return $out."</ul></div>";
}  
  

 function printGear($gear) {
   echo "<h3>".$gear['name']."</h3>";
   echo "<h5>Weight: ".$gear['weight']."</h5>";
 }
 
function printModulePicker($gear) {
  global $db;
  $category = ModuleCategory::select($db->filter(array()));
  $out = "<div class='dropdown'>
  <button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>Add to Module
  <span class='caret'></span></button><ul class='dropdown-menu multi-level'>";
  foreach($category AS $c){
    $out .= "<li class='dropdown-submenu'>
    <a href='#'>".$c['name']."</a>
    <ul class='dropdown-menu'>";
    foreach($c['modules'] AS $m){
      $out .= "<li><a href='manageModules.php?type=create_link&gear_id={$gear['id']}&module_id={$m['id']}'>".$m['name']."</a></li>";
    }
    $out .= "</ul></li>";
  }
  return $out."</ul>";
}

function printCategoryPicker($gear) {
  global $db;
  $category = ModuleCategory::select($db->filter(array()));
  $out = "<div class='dropdown'>
  <button class='btn btn-default dropdown-toggle' type='button' data-toggle='dropdown'>Select Category
  <span class='caret'></span></button><ul class='dropdown-menu multi-level'>";
  foreach($category AS $c){
    $out .= "<li>";
    $out .= "<a href='manageModules.php?type=gear_category&gear_id={$gear['id']}&category_id={$c['id']}'>".$c['name']."</a>";
    $out .= "</li>";
  }
  return $out."</ul>";
}

function printPackButton($module,$pack) {
  global $db;
  $out = "<a href='manageModules.php?type=create_link_module_pack&pack_id={$pack['id']}&module_id={$module['id']}'>Add to ".$pack['name']."</a>";
  return $out;
}

function printModuleButton($module,$gear) {
  global $db;
  $out = "<div id='{$module['id']}-{$gear['id']}' onclick='createLinkModuleGear({$module['id']},{$gear['id']})'>Add to ".$module['name']." ".$module['category']['name']." module</div>";
  return $out;
}
  
function printGearTable($gear, $options=array()) {
?>
  <table id="gear-table" class ="table-sort table table-sum">
    <thead>
      <tr>
        <th>Brand</th>
        <th>Name</th>
        <th>Weight</th>
<?php
      if(isset($options['category'])) {  
        echo "<th>Category</th>";
      }
      if(isset($options['picker'])) {
        echo "<th>Module</th>";
      }
      if(isset($options['carry'])) {  
        echo "<th class='carry'>Carry</th>";
      }  
      if(isset($options['delGear'])) {  
        echo "<th>Delete</th>";
      }  
      
?>
      </tr>
    </thead>
    
    <tbody>
<?php
    foreach($gear AS $g) {
      $rows = "";
      if(isset($options['addToModule'])) {
        $rows .= "<tr class='pointer' id='{$options['addToModule']['id']}-{$g['id']}' onclick='createLinkModuleGear({$options['addToModule']['id']},{$g['id']})'>";
      } else {
        $rows .= "<tr>";
      }
      if(isset($options['addToModule'])) {
        $rows .= "<td>{$g['brand']['name']}</td>
        <td>{$g['name']}</td>
        <td class='weight'>".number_format($g['weight'],2)."</td>";
      } else {
        $rows .= "<td><a href='gear.php?brand_id={$g['brand_id']}'>{$g['brand']['name']}</a></td>
        <td>"./*<a href='gear.php?id={$g['id']}'>*/$g['name']/*</a>*/."</td>
        <td class='weight'>".number_format($g['weight'],2)."</td>";
      }
      
      if(isset($options['categoryPicker'])) {
        $rows .= "<td>".printCategoryPicker($g)."</td>";
      } else if(isset($options['category'])) { 
        if(isset($options['addToModule'])) {
          $rows .= "<td>{$g['category']['name']}</td>";
        } else {
          $rows .= "<td><a href='gear.php?category_id={$g['category_id']}'>{$g['category']['name']}</a></td>";  
        }
      }
      if(isset($options['addToModule'])) {
        //$rows .= "<td>".printModuleButton($options['addToModule'],$g)."</td>";
      } else {
        if(isset($options['picker'])) {  
          $rows .= "<td>".printModulePicker($g)."</td>";
        }
        if(isset($options['carry'])) {
          $rows .= "<td class='carry'><input class='checkbox' type='checkbox' checked='checked' onchange='calculateWeights()'></td>";
        }
        if(isset($options['delGear'])) {  
          $rows .= "<td><a class='text-danger' href='manageModules.php?type=del_module_gear&module_id=".$options['delGear']['id']."&gear_id=".$g['id']."'>Delete</a></td>";
        }  
      }
      $rows .= "</tr>";
      echo $rows;
    }
?>
    </tbody>
    </table>
<?php
}

function printModule($module, $options=array()) {
  ?>

  <div id='{$module['id']}-module' class='table-sum'>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <h3><?=$module['name']?> <?=$module['category']['name']?></h3>
      </div>
      <div class="col-xs-6">
        <h3 class=''>
          <a class='btn btn-default  fa fa-clone' data-toggle="tooltip" title="Clone"></a>
          <a class='btn btn-default  fa fa-share' data-toggle="tooltip" title="Share"></a>
        </h3>
      </div>
    </div>
    <h5>Module Weight: <span class='total'></span> oz</h5>
<?php 
  printGearTable($module['gear'], $options);
?>
  </div>
<?php
    if(isset($options['module_add_gear'])) {
      echo "<a href='gear.php?module_id=".$module['id']."'>Add gear to ".$module['name']." ".$module['category']['name']."</a>";
    }
}
  
function printModulePanel($module, $options=array()) {
  $random = rand(0,2000);
?>

  <div id='<?=$module['id']?>-module' class="table-sum col-lg-4 col-md-6 col-xs-12 grid-item">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title row">
          <div class="col-xs-8">
            <a href="module.php?id=<?=$module['id']?>"><?=$module['name']." ".$module['category']['name']?></a>
          </div>
          
          <div class="module-options col-xs-4">
<!--            <span class="glyphicon glyphicon-share" ></span>
            <span class="glyphicon glyphicon-floppy-disk"></span> -->
            <?php
              if(isset($options['pack'])) {
            ?>
            <a class="text-muted hover-red" href="manageModules.php?type=del_pack_module&module_id=<?=$module['id']?>&pack_id=<?=$options['pack']['id']?>" onclick="return confirm('Remove this module from your pack?')"><span class="glyphicon glyphicon-remove"></span></a>
            <?php
              } else if(isset($options['purchaseModule'])) {
            ?>
                <a class="text-muted hover-red" href="#<?=$module['id']?>-module" onclick="return confirm('Purchase this item?')"><span class="glyphicon glyphicon-shopping-cart"></span></a>
            <?php
              } else if(isset($options['owner']) && isset($options['delModule'])) {
            ?>
                <a class="text-muted hover-red" href="manageModules.php?type=del_module&module_id=<?=$module['id']?>" onclick="return confirm('Delete this module forever?')"><span class="glyphicon glyphicon-remove"></span></a>
            <?php
              }
            ?>
          </div>
        </h4>
        <?php
            if(isset($options['owner']) && isset($options['pack']) && isset($options['addToPack'])) {
              echo printPackButton($module,$options['pack']);
            }
            ?>
      </div>
      <div class="panel-body">
        <h5>Module Weight: <span class="total"></span> oz</h5>
<?php
/*  $options["carry"] = 0;
  if(isset($options['owner']) && isset($options['module'])) {
    $options["module"]=$module;
  } */
  printGearTable($module['gear'], $options);
  if(isset($options['owner']) && isset($options['module_add_gear'])) {
      echo "<a href='gear.php?module_id=".$module['id']."'>Add gear to ".$module['name']." ".$module['category']['name']."</a>";
    }
    if(isset($options['user_name'])) {
      echo "<h5><em>Contributed by ".$module['user']['name_first']." ".$module['user']['name_last']."</em></h5>";
    }
?>

      </div>
    </div>
  </div>
<?php
}//END printModule function

function createNewCategoryForm() {
  global $db;
  ?>
  <h3>Missing a Gear Category?</h3>
  <form action='manageModules.php' method="POST">
    <div class="form-group">
  		<input type="text" placeholder="Name" class="form-control" name="name" required>
  	</div>
  	<input type='hidden' name='type' value='create_category'>
    <input class="btn btn-default" type="submit" name="submit" value="Submit">
  </form>
  <?php
}


function createNewBrandForm() {
  global $db;
  ?>
  <h3>Missing one of your favorite Brands?</h3>
  <form action='manageModules.php' method="POST">
    <div class="form-group">
  		<input type="text" placeholder="Name" class="form-control" name="name" required>
  	</div>
  	<input type='hidden' name='type' value='create_brand'>
    <input class="btn btn-default" type="submit" name="submit" value="Submit">
  </form>
  <?php
}


function createNewGearForm() {
  global $db;
  ?>
  <h3>Missing your favorite gear?</h3>
  <form action='manageModules.php' method="POST">
    <div class="form-group">
  		<input type="text" placeholder="Name" class="form-control" name="name" required>
  	</div>
  	<div class="form-group">
  		<input type="number" placeholder="weight (oz)" class="form-control" name="weight" required>
  	</div>
    <div class="form-group">
    	<select class="form-control" name="brand_id" required>
      	<option value="" disabled selected>Brand</option>
    		<?php
          $category = Brand::select($db->filter()->orderby("name ASC"));
          foreach($category AS $c) {
            echo "<option value='{$c['id']}'>".$c['name']."</option>";
          }
        ?>
    	</select>
    </div>
    <div class="form-group">
    	<select class="form-control" name="category_id" required>
      	  <option value="" disabled selected>Category</option>
    		<?php
          $category = ModuleCategory::select($db->filter()->orderby("name ASC"));
          foreach($category AS $c) {
            echo "<option value='{$c['id']}'>".$c['name']."</option>";
          }
        ?>
    	</select>
    </div>

  	<input type='hidden' name='type' value='create_gear'>
    <input class="btn btn-default" type="submit" name="submit" value="Submit">
  </form>
  <?php
}

function printModuleAddPanel($pack) {
?>
<div onclick="location.href='module.php?pack_id=<?=$pack['id']?>';" class="col-lg-4 col-md-6 col-xs-12 grid-item pointer">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-sm-12 faded-font">
          Module Title
        </div>
      </h4>
    </div>
    <div class="panel-body">
      <h5 class="faded-font">Module Weight: 0.00 oz</h5>
      <div class="row">
        <div class="col-xs-3">
        </div>
        <div class="add-module col-xs-6">Add Module</div>
      </div>
      
    </div>
  </div>
</div>
<?php
}

function printCreateNewGear() {
  ?>
  <a href="newGear.php" class='btn btn-default'>Create New Gear</a>
  <?php
}

function printModules($modules, $options=array()) {
  echo "<div class='grid'>";
  if(isset($options['addModule'])) {
    printAddModule();
  }
  foreach ($modules AS $m) {
    printModulePanel($m, $options);
  }
  echo "</div>";
}

function printPack($pack, $options=array()) {
  echo "<div id='pack{$pack['id']}' class='pack-wrapper'>";
  ?>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <h3><?=$pack['name']?></h3>
      </div>
      <div class="col-xs-6">
        <h3 class=''>
          <a class='btn btn-default  fa fa-clone' data-toggle="tooltip" title="Clone"></a>
          <a class='btn btn-default  fa fa-share' data-toggle="tooltip" title="Share"></a>
          
<?php
  if(isset($options['owner'])){
?>
    <form class='inline-form' id='delPack' action='manageModules.php' method='POST'>
    <input type='hidden' name='pack_id' value='<?=$pack['id']?>'>
    <input type='hidden' name='type' value='del_pack'>
    <button onclick="return confirm('Delete this pack forever?')" class='btn btn-default  fa fa-close' data-toggle='tooltip' title='Delete'></button>
    </form>
<?php
  }   
?>
        </h3>
      </div>
    </div>

  <?php
  echo "<h5>Total Weight: <span class='grand-total'></span></h5>";
  echo "<div class='grid'>";
  if(isset($options['moduleAdd'])) {
    printModuleAddPanel($pack);
  }
  foreach ($pack['modules'] AS $m) {
    printModulePanel($m,$options);
  }
  echo "</div></div>";
}

function printPackOverview($pack,$options=array()) {
?>
<div class='col-lg-4 col-md-6 col-xs-12 grid-item'>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-sm-12 faded-font">
  <?php
  echo "<a href='pack.php?id={$pack['id']}'>{$pack['name']}</a>";
  ?>
       </div>
      </h4>
    </div>
    <div class="panel-body">
  <?php
  echo "<h5>Total Weight: <span class='grand-total'>{$pack['weight']} oz ({$pack['weight_lb']} lbs)</span></h5>";
  DTLog::debug($pack['user']);
  echo "<p class='justify'>{$pack['description']}</p>";
  if(isset($options['user_name'])) {
    echo "<h5><em>Contributed by ".$pack['user']['name_first']." ".$pack['user']['name_last']."</em></h5>";
  }
  ?>
    </div>
  </div>
</div>
<?php
}

function printAddPack() {
?>
<div class="col-lg-4 col-md-6 col-xs-12 grid-item">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-sm-12 faded-font">
          Create New Pack
        </div>
      </h4>
    </div>
    <div class="panel-body">
      <form action='manageModules.php' method="POST">
        <div class="form-group">
      		<input type="text" placeholder="Name" class="form-control" name="name" required>
      	</div>
        <div class="form-group">
      		<textarea placeholder="Description" class="form-control max-width" name="description" required></textarea>
      	</div>
      	<input type='hidden' name='type' value='create_pack'>
        <input class="btn btn-default" type="submit" name="submit" value="Submit">
      </form>
    </div>
  </div>  
</div>
<?php  
}

function printAddModule() {
  global $db;
?>
<div class="col-lg-4 col-md-6 col-xs-12 grid-item">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-sm-12 faded-font">
          Create New Module
        </div>
      </h4>
    </div>
    <div class="panel-body">
      <form action='manageModules.php' method="POST">
        <div class="form-group">
      		<input type="text" placeholder="Name" class="form-control" name="name" required>
      	</div>
      	<div class="form-group">
      		<select class="form-control" name="module_category_id" required>
        		<option value="" disabled selected>Select a Category</option>
        		<?php
              $category = ModuleCategory::select($db->filter());
              foreach($category AS $c) {
                echo "<option value='{$c['id']}'>".$c['name']."</option>";
              }
            ?>
      		</select>
      	</div>
      	<input type='hidden' name='type' value='create_module'>
        <input class="btn btn-default" type="submit" name="submit" value="Submit">
      </form>
    </div>
  </div>
</div>

<?php  
}

function printPacks($packs, $options=array()) {
  $top = "";
  $bottom = "";
  if(isset($options['overview'])) {
    $top = "<div class='grid'>";
    $bottom = "</div>";
  }
  echo $top;
  if(isset($options['owner'])) {
    printAddPack();
  }
  foreach($packs AS $pack) {
    if(isset($options['overview'])) {
        printPackOverview($pack,$options);
    }
    else if(isset($options['edit'])) {
      $options['pack']=$pack;
      printPack($pack,$options);
    } else {
      printPack($pack,$options);
    }
  }
  echo $bottom;
}

?>