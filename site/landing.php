<?php
session_start();
require('gear.inc.php');
include $prepage;
	
$oauth_token = $params->stringParam("oauth_token");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/gear.css">
  <script src="js/jquery-2.2.0.min.js"></script>
  <script src="js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body class="army-green">  
 <div class="container-full">

      <div class="row">
       
        <div class="col-lg-12 text-center v-center">
          
          <h1 class="landing-title">Gear Crossing</h1>
          <p class="lead"><em>optimize your pack</em></p>
        
        </div>
        
      </div> <!-- /row -->
  
  	  <div class="row">
       
        <div class="col-lg-12 text-center" style="font-size:39pt;">
          <a class="btn btn-outline" href="users/register.php">Sign Up</a>
          <a class="btn btn-outline" href="login.php?oauth_token=<?=$oauth_token?>">Sign In</a>
        </div>
        <div class="col-lg-12 text-center v-center" style="font-size:39pt;">
          <a href="#"><i class="fa fa-twitter-square"></i></a> <a href="#"><i class="fa fa-facebook-square"></i></a>  <a href="#"><i class="fa fa-google-plus-square"></i></a> <a href="#"><i class="fa fa-github-square"></i></a> <a href="#"><i class="fa fa-pinterest-square"></i></a>
        </div>
      
      </div>
  
  	<br><br><br><br><br>

</div><?php
include $postpage;
?>
