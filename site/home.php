<?php
require_once dirname(__FILE__)."/gear.inc.php";
require($prepage);

$session = DTSession::sharedSession();
$userConsumer = new DTProviderConsumer("gear","users.php",$dt_token);
$newUsers=$userConsumer->request("newThisWeek",array("id"=>$user['id']));
?>
<div class='col-lg-4 col-md-3 col-xs-0'></div>
<div class='container-fluid'>
  <h4><?=$newUsers['count']?> new hikers this week</h4>

<?php
$moduleConsumer = new DTProviderConsumer("gear","module.php",$dt_token);
$newModules=$moduleConsumer->request("newThisWeek",array("id"=>$user['id']));  
?>
  <h4><?=$newModules['count']?> new modules this week</h4>

<?php
$packConsumer = new DTProviderConsumer("gear","pack.php",$dt_token);
$newPacks=$packConsumer->request("newThisWeek",array("id"=>$user['id']));  
?>
  <h4><?=$newPacks['count']?> new packs this week</h4>

</div>
<div class='col-lg-4 col-md-3 col-xs-0'></div>

<?php
	include $postpage;