<?php
  
require($prepageAPI);

// redirect to HTTPS as soon as possible, to avoid "header already sent" and unnecessary loading
// this will trip up invalid cert calls to provider... this should be done by the provider redirect

// handle logging out
if($params->stringParam("action")=="logout"){
	$consumer = new DTProviderConsumer("gear","authentication.php",$dt_token); //any old provider
	$consumer->request("session_destroy");
	unset($_COOKIE["login_method"]);
	setcookie("login_method",null,-1);
	DTSession::destroy();
	header("Location: ".DTSettingsConfig::baseURL("index.php"));
	exit();
}

//require('functions.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?=DTSettingsConfig::baseURL("css/bootstrap.min.css")?>">
<link rel="stylesheet" href="<?=DTSettingsConfig::baseURL("css/font-awesome.min.css")?>">
<link rel="stylesheet" href="<?=DTSettingsConfig::baseURL("font-awesome-4.5.0/css/font-awesome.min.css")?>">

<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/jquery-2.2.0.min.js")?>"></script>
<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/gearcrossing.js")?>"></script>
<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/bootstrap.min.js")?>"></script>
<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/masonry.pkgd.js")?>"></script>
<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/jquery.tablesorter.min.js")?>"></script>

	<!-- bootstrap validator -->
	<link rel="stylesheet" href="<?=DTSettingsConfig::baseURL("js/formvalidation/css/formValidation.min.css")?>" />
	<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/formvalidation/js/formValidation.min.js")?>"></script>
	<script type="text/javascript" src="<?=DTSettingsConfig::baseURL("js/formvalidation/js/framework/bootstrap.min.js")?>"></script>

	<!-- select2 -->
	<link href="<?=DTSettingsConfig::baseURL("vendor/expressive-analytics/select2/select2.css")?>" rel="stylesheet" />
	<link href="<?=DTSettingsConfig::baseURL("vendor/expressive-analytics/select2/select2-bootstrap.css")?>" rel="stylesheet" />
	<script src="<?=DTSettingsConfig::baseURL("vendor/expressive-analytics/select2/select2.js")?>"></script>

	<!-- Deep Thought -->
	<script src="<?=DTSettingsConfig::baseURL("js/deep-thought.js")?>"></script>
	<script src="<?=DTSettingsConfig::baseURL("js/jquery.iframe-transport.js")?>"></script>
    <script src="<?=DTSettingsConfig::baseURL("js/utils.js")?>"></script>
    
    <!-- Glyphicons -->
	<link href="<?=DTSettingsConfig::baseURL("css/glyphicons.css")?>" rel="stylesheet" />
	<link href="<?=DTSettingsConfig::baseURL("css/glyphicons-halflings.css")?>" rel="stylesheet" />
	<link href="<?=DTSettingsConfig::baseURL("css/glyphicons-social.css")?>" rel="stylesheet" />
  <link rel="stylesheet" href="<?=DTSettingsConfig::baseURL("css/gear.css")?>" rel="stylesheet">
</head>
<body>
	<?php require __DIR__."/js/dynamic.js.php"; ?>