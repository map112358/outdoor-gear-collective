<?php
require_once dirname(__FILE__)."/gear.inc.php";
require($prepage);

// technically, the user object is already requested on the prepage, if we're
// already logged in. However, since this is our "sign in" target, we force a
// secure request, to get us logged in
$consumer = new DTSecureProviderConsumer("gear","secure/users.php",$dt_token);
$profile = $consumer->request("current_user",array());

DTLog::debug($profile);

?>
<div class='container'>
  <div class="">
  <div class="">
    <div class="avatar">
      <img alt="" src="img/<?=$profile['image']['name']?>" onerror="if (this.src != 'img/default_user.png') this.src = 'img/default_user.png';">
    </div>
    <h2><?=$profile['name_first']?> <?=$profile['name_last']?></h2>
  </div>

  <h3>Packs</h3>
  <a href="pack.php?user_id=<?=$profile['id']?>"><em>View All</em></a>
<?php  
  printPacks($profile['packs'],array("overview"=>true));
?>
  </div>
  <div class="">
    <h3>Modules</h3>
    <a href="module.php?user_id=<?=$profile['id']?>"><em>View All</em></a>
<?php
  $modules = Module::select($db->filter(array("user_id"=>$profile['id']))->orderby("RANDOM()")->limit("6"));
  printModules($modules);
?>
  </div>
  <div class="">
    <h3>Gear</h3>
    <a href="gear.php?user_id=<?=$profile['id']?>"><em>View All</em></a>
<?php
  $gear = Gear::select($db->filter(array("user_id"=>$profile['id']))->orderby("RANDOM()")->limit("10"));
  printGearTable($gear);
?>
  </div>
</div>

<?php
/*$users = Users::select($db->filter()->where("id <> ".$user['id']));
  foreach($users AS $u) {
    printUserCard($u);
  }*/
?>

<script>
$(document).ready(function() {
  resizeTextAreaEvent();
  fresh();
});
</script>

<?php
require($postpage);