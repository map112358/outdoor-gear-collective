<?php
	class UserProfileView extends ViewController {
		public function renderView($obj){
			$dt_token = DTAPI::consumerTokenForAPI("gear");
			$user_consumer_url = DTSettingsConfig::baseURL("consumers/users_consumer.php");
			return <<<END
	<fieldset>
		<legend>Basic Information</legend>
		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<label for="alias">Name</label>
					<input type="text" class='form-control' id="alias" name="alias" value="{$obj["alias"]}" placeholder="Username" required
						data-fv-remote=true
						data-fv-remote-data='{"act":"valid_user","tok":"{$dt_token}","fmt":"json"}'
						data-fv-remote-delay=1000
						data-fv-remote-message="This username is already registered."
						data-fv-remote-url="{$user_consumer_url}"
						data-fv-regexp="true"
		                data-fv-regexp-regexp="^[a-z0-9_\-]+$"
		                data-fv-regexp-message="Username cannot contain uppercase, spaces, or special characters."
					/>
				</div>
			</div>
			<div class='col-md-6'>
				<div class='form-group'>
					<label for="email">Email</label>
					<input type="email" class='form-control' id="email" name="email" placeholder="Email Address" value="{$obj["email"]}" required />
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<label for="password">New Password</label>
					<input type="password" class='form-control' id="password" name="password" placeholder="New Password" />
				</div>
			</div>
			<div class='col-md-6'>
				<div class='form-group'>
					<label for="verify">Verify</label>
					<input type="password" class='form-control' id="verify" name="verify" placeholder="Re-enter Password" data-fv-identical="true" data-fv-identical-field="password" data-fv-identical-message="must match password" />
				</div>
			</div>
		</div>
	</fieldset>
END;
		}
		
		public function renderAdminView($obj){
			$is_admin = $obj["is_admin"]?"checked":"";
			
			return <<<END
	<fieldset>
		<legend>Privileges</legend>
		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<label for="role_id">Default Role</label>
					<select class='form-control' id="role_id" name="role_id"></select>
				</div>
			</div>
			<div class='col-md-6'>
				<div class='form-group'>
					<label for="is_admin">Super User</label>
					<input type="checkbox" class='form-control switch' id="is_admin" name="is_admin" {$is_admin} />
				</div>
			</div>
		</div>
	</fieldset>
END;
		}
		
		function renderUserCard($usersName,$page=1) {
      $header = "background: url('".DTSettingsConfig::baseURL("uploads/".$usersName['image_header']['filename'])."'), url('".DTSettingsConfig::baseURL("img/banner-test.jpg")."'); background-repeat: no-repeat; background-size:cover;";
      $profileLink = "location.href='". DTSettingsConfig::baseURL("users/profile.php?id={$usersName['id']}") ."'";
      $imageLink = DTSettingsConfig::baseURL("uploads/".$usersName['image']['filename']);
      $defaultImageLink = DTSettingsConfig::baseURL("img/default_user.png");
      $out = <<<END
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 page{$page}" onclick="{$profileLink}">
      <div class="card hovercard pointer">
          <div class="cardheader" style="{$header}">
      
          </div>
          <div class="avatar">
            <img alt="" src="{$imageLink}" onerror="if (this.src != '{$defaultImageLink}') this.src = '{$defaultImageLink}';">
          </div>
          <div class="info">
            <div class="title">{$usersName['name_first']} {$usersName['name_last']}</div>
          </div>
          <div class="stats container-fluid">
          <h5 class="stats-title">User Stats</h5>
            <div class="col-xs-3">
              <h6>Gear</h6>
              {$usersName['stats']['gear_count']}
            </div>
            <div class="col-xs-3">
              <h6>Modules</h6>
              {$usersName['stats']['module_count']}
            </div>
            <div class="col-xs-3">
              <h6>Packs</h6>
              {$usersName['stats']['pack_count']}
            </div>
            <div class="col-xs-3">
              <h6>Followers</h6>
              {$usersName['stats']['follower_count']}
            </div>
          </div>
          <div class="bottom">
          </div>
      </div>
    </div>
END;
      return $out;
    }
		
	}