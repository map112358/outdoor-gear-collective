<?php
	class PackView extends ViewController {
		public function renderOverview($pack, $page=1){
			$str = "";
		  if(isset($options['user_name'])) {
		  	$str = "";
			}
			$out = <<<END
<div class='col-lg-6 col-md-6 col-xs-12 grid-item page{$page}'>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-sm-12 faded-font">
          <a href=
END;
      $out .= '"'. DTSettingsConfig::baseURL("packs/profile.php?id={$pack['id']}") .'"';
      $out .= <<<END
>{$pack['name']}</a>
       </div>
      </h4>
    </div>
    <div class="panel-body">
		<h5>Total Weight: <span class='grand-total'>{$pack['weight']} oz ({$pack['weight_lb']} lbs)</span></h5>
		<p class='justify'>{$pack['description']}</p>
		{$str}
		<h5 class="pull-right"><em>Contributed by {$pack['user']['name_first']} {$pack['user']['name_last']}</em></h5>
    </div>
  </div>
</div>
END;
      return $out;
		}
		
		public function renderAdminOverview($pack, $user, $page=1){
			$str = "";
		  if(isset($options['user_name'])) {
		  	$str = "";
			}
			$out = <<<END
<div class='col-lg-6 col-md-6 col-xs-12 grid-item page{$page}'>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-xs-10">
          <a href=
END;
      $out .= '"'. DTSettingsConfig::baseURL("packs/profile.php?id={$pack['id']}") .'"';
      $out .= <<<END
>{$pack['name']}</a>
        </div>
        <div class="col-xs-2">
          <a class="fa fa-times pointer pull-right" data-toggle="tooltip" title="Close"
END;
      $dt_token = DTAPI::consumerTokenForAPI("gear");
      $out .= " onclick=\"deletePack('".DTSettingsConfig::baseURL("consumers/secure/pack_consumer.php")."','".$dt_token."',".$pack['id'].")\">";
      $out .= <<<END
</a>
        </div> 
      </h4>
    </div>
    <div class="panel-body">
		<h5>Total Weight: <span class='grand-total'>{$pack['weight']} oz ({$pack['weight_lb']} lbs)</span></h5>
		<p class='justify'>{$pack['description']}</p>
		{$str}
		<h5 class="pull-right"><em>Contributed by {$pack['user']['name_first']} {$pack['user']['name_last']}</em></h5>
    </div>
  </div>
</div>
END;
      return $out;
		}
		
		public function renderProfile($pack,$secure=false){
			$out = <<<END
<div class="container">
  <div class="row pack-container">
    <div class="col-xs-12">
      <h3>{$pack['name']}</h3>
      <h4>Weight: <span class='grand-total'>{$pack['weight']}</span></h4>
      <h5><em>Contributed by {$pack['user']['name_first']} {$pack['user']['name_last']}</em></h5>
END;
      if($secure)
        $out .= "<div class='form-group'><button data-toggle='modal' data-target='#addModulesModal' class='btn btn-green' href='#'>Manage Modules</button></div>";
      $out .= "<div class='row grid'>";
      $pView = new VCList(new DTParams());
      $pView->setItems($pack['modules']);
      foreach($pView->items AS $obj) {
        //DTLog::debug($pack);
      	$moduleView = new ModuleView();
      	$out .= $moduleView->renderPackAdminOverview($obj,$pack);
      }
      $out .= "</div>";
      if($secure)
        $out .= "<div class='form-group'><button data-toggle='modal' data-target='#addGearPackModal' class='btn btn-green' href='#'>Manage Gear</button></div>";
      $out .= "<div class='table-sum'>";
      $gView = new GearListView(new DTParams());
      $gView->setItems($pack['gear']);
      if($gView->renderView($pack['gear']) != "")
        $out .= "<h5>Stray Gear Weight: <span class='total'></span> oz</h5>";
      $out .= $gView->renderView($pack['gear']);
      $out .= "</div>";
      
      $out .= <<<END
    </div>
  </div>
END;
      
      if($secure){
        $pv = new PackView();
        //echo $pv->renderLinkModule();
      }
      if($secure) {
        $search_consumer = DTSettingsConfig::baseURL("consumers/module_consumer.php");
        $dt_token = DTAPI::consumerTokenForAPI("gear");
        $out .= <<<END
        <div id='addModulesModal' class='modal fade module-pack-modal'>
          <div class="modal-dialog">
  
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                
                <h4 class="modal-title">Module Selector</h4>
              </div>
              <div class="modal-body">
                <div class="form-group col-xs-12">
              		<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchModulePackModals('{$search_consumer}', '{$dt_token}', {$pack['id']}, '#modal-module-pack', $(this).val().toLowerCase() )">
              	</div>
                <div id="modal-module-pack" class="container-fluid">
END;
        $dt_token = DTAPI::consumerTokenForAPI("gear");
        $mConsumer = new DTProviderConsumer("gear","module.php",$dt_token);
        $mView = new VCList(new DTParams());
        $mView->requestItems($mConsumer);
        foreach($mView->items as $obj){
        	$moduleView = new ModuleView();
        	$out .= $moduleView->renderModalOverview($obj, $pack);
        }
        $search_consumer = DTSettingsConfig::baseURL("consumers/gear_consumer.php");
        $out .= <<<END
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
              </div>
            </div>
          </div>
        </div>
        <div id='addGearPackModal' class='modal fade gear-pack-modal'>
          <div class="modal-dialog">
  
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                <h4 class="modal-title">Gear Selector</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
              		<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchGearPackModals('{$search_consumer}', '{$dt_token}', {$pack['id']}, '#modal-gear-pack-table', $(this).val().toLowerCase() )">
              	</div>
END;
        $consumer = new DTProviderConsumer("gear","gear.php",$dt_token);
        $gView = new GearListView(new DTParams());
        $gView->requestItems($consumer);
        $out .= $gView->renderModalView($pack);
        $out .= <<<END
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
              </div>
            </div>
          </div>
        </div>
END;
      }
      
      $out .= <<<END
</div>
END;
      return $out;
		}		
		
		public function renderAddPack($user_id, $dt_token){
			$out = <<<END
  <div class="col-lg-6 col-md-6 col-xs-12 grid-item">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title row">
          <div class="col-sm-12">
            <a>Create New Pack</a>
          </div>
        </h4>
      </div>
      <div class="panel-body">
        <form class='form pack-form' role="form" action='javascript:dt.post({"url":
END;
      $out .= '"'. DTSettingsConfig::baseURL("consumers/pack_consumer.php") .'"';
      $out .= ',"success":function(){window.location.reload()}';
      $out .= <<<END
,"form":".pack-form"})'>
          <input type="hidden" name="user_id" value="{$user_id}" />
          <input type="hidden" name="act" value="addpack" />
				  <input type="hidden" name="tok" value="{$dt_token}" />
          <div class="form-group">
        		<input type="text" placeholder="Name" class="form-control" name="name" required>
        	</div>
          <div class="form-group">
        		<textarea placeholder="Description" class="form-control max-width" name="description" required></textarea>
        	</div>
        	<input type='hidden' name='type' value='create_pack'>
          <input type="submit" class='btn btn-green pull-right' value="Save" />
        </form>
      </div>
    </div>  
  </div>
END;
      return $out;
		}
	}