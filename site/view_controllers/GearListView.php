<?php
	class GearListView extends VCList {
		public function renderView(){
  		$gView = new GearView();
			$str = "";
			$random = rand(0,2000);
			$out = <<<END
  <table id="gear-table" class ="table-sort table table-sum">
    <thead>
      <tr>
        <th>Brand</th>
        <th>Name</th>
        <th>Weight</th>
      </tr>
    </thead>
    <tbody>
END;
      $builder_f = function($g) use ($gView){
				return $gView->renderRow($g);
			};
			if($this->renderEach($builder_f) == "")
			  return "";
      $out .= $this->renderEach($builder_f);
      $out .= <<<END
    </tbody>
    </table>
END;
      return $out;
		}
		
    public function renderEditView(){
  		$gView = new GearView();
			$str = "";
			$random = rand(0,2000);
			$out = <<<END
  <table id="gear-table" class ="table-sort table table-sum">
    <thead>
      <tr>
        <th>Brand</th>
        <th>Name</th>
        <th>Weight</th>
      </tr>
    </thead>
    <tbody>
END;
      $builder_f = function($g) use ($gView){
				return $gView->renderRow($g);
			};
			if($this->renderEach($builder_f) == ""){
		    return $gView->renderAddGearPanel();
		  }
      $out .= $this->renderEach($builder_f);
      $out .= <<<END
    </tbody>
    </table>
END;
      return $out;
		}		
		
		public function renderPage(){
  		$gView = new GearView();
			$str = "";
			$random = rand(0,2000);
      $builder_f = function($g) use ($gView){
				return $gView->renderRow($g);
			};
			if($this->renderEach($builder_f) == "")
			  return "";//$gView->renderAddGearPanel();
      $out = $this->renderEach($builder_f);
      return $out;
		}
		
    public function renderModalView($moduleOrPack){
  		$gView = new GearView();
			$str = "";
			$random = rand(0,2000);
			$domID = "modal-gear-module-table";
			if(isset($moduleOrPack['modules']))
			  $domID = "modal-gear-pack-table";
			$out = <<<END
  <table id="{$domID}" class ="table-sort table table-sum searchable-table">
    <thead>
      <tr>
        <th></th>
        <th>Brand</th>
        <th>Name</th>
        <th>Weight</th>
      </tr>
    </thead>
    <tbody>
END;
      $builder_f = function($g) use ($gView, $moduleOrPack){
				return $gView->renderModalRow($g, $moduleOrPack);
			};
			if($this->renderEach($builder_f) == "")
			  return $gView->renderAddGearPanel();
      $out .= $this->renderEach($builder_f);
      $out .= <<<END
    </tbody>
    </table>
END;
  return $out;
		}
		
    public function renderModalRowView($moduleOrPack){
  		$gView = new GearView();
			$str = "";
			$random = rand(0,2000);
      $builder_f = function($g) use ($gView, $moduleOrPack){
				return $gView->renderModalRow($g, $moduleOrPack);
			};
      return $this->renderEach($builder_f);
		}
	}