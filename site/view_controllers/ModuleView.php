<?php
  require_once dirname(__FILE__)."/../gear.inc.php";
	
	class ModuleView extends ViewController {
		public function renderOverview($module, $page=1){
			$str = "";
			$random = rand(0,2000);
			$out = <<<END
  <div id='{$module["id"]}-module' class="table-sum col-lg-6 col-md-6 col-xs-12 grid-item page{$page}">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title row">
          <div class="col-sm-10 col-xs-9">
            <a href=
END;
      $out .= '"'. DTSettingsConfig::baseURL("modules/profile.php?id={$module["id"]}") .'"';
      $out .= <<<END
><span class="black">{$module['category']['name']}</span> {$module['name']}</a>
            
          </div>
END;
/*          <div class="col-sm-2 col-xs-3 pull-right">
            <a class="fa fa-clone pointer" data-toggle="tooltip" title="Clone"></a>
            <a class="fa fa-share pointer" data-toggle="tooltip" title="Share"></a>
          </div> */
      $out .= <<<END
        </h4>
      </div>
      <div class="panel-body">
        <h5>Module Weight: <span class="total"></span> oz</h5>
END;
      $gView = new GearListView(new DTParams());
      $gView->setItems($module['gear']);
      $out .= $gView->renderView($module['gear']);
      $out .= <<<END
        <h5 class="pull-right"><em>Contributed by {$module['user']['name_first']} {$module['user']['name_last']}</em></h5>
      </div>
    </div>
  </div>
END;
      return $out;
		}
		
		public function renderModalOverview($module,$pack){
			$str = "";
			$random = rand(0,2000);
			$out = <<<END
  <div class="row searchable-container">
  <div id='{$module["id"]}-module' class="table-sum col-xs-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title row">
          <div class="col-xs-8 searchable">
            <a><span class="black">{$module['category']['name']}</span> {$module['name']}</a>
          </div>
          <div class="pull-right">
END;
        $dt_token = DTAPI::consumerTokenForAPI("gear");
        
        //
        //check if this module is currently in the pack
        //
        
        $hasModule = false;
        foreach($pack['modules'] AS $m) {
          if($m['id']==$module['id']){
            $hasModule = true;
          }
        }
        
        //checkmark for in_pack, and plus otherwise
        if($hasModule) {
          $out .= "<span id=\"module{$module['id']}\" class='fa fa-check green addRemoveToggle pointer padding-right-xs' onclick=\"removeModuleFromPack('".DTSettingsConfig::baseURL("consumers/secure/pack_consumer.php")."','".$dt_token."',".$module['id'].",".$pack['id'].")\"></span>"; 
        } else {
          $out .= "<span id=\"module{$module['id']}\" class='fa fa-plus addRemoveToggle pointer padding-right-xs' onclick=\"addModuleToPack('".DTSettingsConfig::baseURL("consumers/secure/pack_consumer.php")."','".$dt_token."',".$module['id'].",".$pack['id'].")\"></span>";
        }
        $out .= <<<END
          </div>
        </h4>
      </div>
      <div class="panel-body">
        <h5>Module Weight: <span class="total"></span> oz</h5>
END;
      $gView = new GearListView(new DTParams());
      $gView->setItems($module['gear']);
      $out .= $gView->renderView($module['gear']);
      $out .= <<<END
        <h5 class="pull-right"><em>Contributed by {$module['user']['name_first']} {$module['user']['name_last']}</em></h5>
      </div>
    </div>
  </div>
  </div>
END;
      return $out;
		}
		
		public function renderAdminOverview($module, $user, $page=1){
			$str = "";
			$random = rand(0,2000);
			$out = <<<END
  <div id='{$module["id"]}-module' class="table-sum col-lg-6 col-md-6 col-xs-12 grid-item page{$page}">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title row">
          <div class="col-sm-10 col-xs-9">
            <a href=
END;
      $out .= '"'. DTSettingsConfig::baseURL("modules/profile.php?id={$module["id"]}") .'"';
      $out .= <<<END
><span class="black">{$module['category']['name']}</span> {$module['name']}</a>
            
          </div>
          <div class="col-sm-2 col-xs-3 ">
            <a class="fa fa-times pointer pull-right" data-toggle="tooltip" title="Close"
END;
      $dt_token = DTAPI::consumerTokenForAPI("gear");
      $out .= " onclick=\"deleteModule('".DTSettingsConfig::baseURL("consumers/secure/module_consumer.php")."','".$dt_token."',".$module['id'].")\">";
      $out .= <<<END
</a>
          </div> 
        </h4>
      </div>
      <div class="panel-body">
        <h5>Module Weight: <span class="total"></span> oz</h5>
END;
      $gView = new GearListView(new DTParams());
      $gView->setItems($module['gear']);
      $out .= $gView->renderView($module['gear']);
      $out .= <<<END
        <h5 class="pull-right"><em>Contributed by {$module['user']['name_first']} {$module['user']['name_last']}</em></h5>
      </div>
    </div>
  </div>
END;
      return $out;
		}
		
    public function renderPackAdminOverview($module, $pack, $page=1){
      //DTLog::debug("in renderAdminModalOverview");
      //DTLog::debug($pack);
			$str = "";
			$random = rand(0,2000);
			$out = <<<END
  <div id='{$module["id"]}-module' class="table-sum col-lg-6 col-md-6 col-xs-12 grid-item page{$page}">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title row">
          <div class="col-sm-10 col-xs-9">
            <a href=
END;
      $out .= '"'. DTSettingsConfig::baseURL("modules/profile.php?id={$module["id"]}") .'"';
      $out .= <<<END
><span class="black">{$module['category']['name']}</span> {$module['name']}</a>
            
          </div>
          <div class="col-sm-2 col-xs-3 ">
            <a class="fa fa-times pointer pull-right" data-toggle="tooltip" title="Close"
END;
      $dt_token = DTAPI::consumerTokenForAPI("gear");
      $out .= " onclick=\"removeModuleFromPackNoToggle('".DTSettingsConfig::baseURL("consumers/secure/pack_consumer.php")."','".$dt_token."',".$module['id'].",".$pack['id'].")\">";
      $out .= <<<END
</a>
          </div> 
        </h4>
      </div>
      <div class="panel-body">
        <h5>Module Weight: <span class="total"></span> oz</h5>
END;
      $gView = new GearListView(new DTParams());
      $gView->setItems($module['gear']);
      $out .= $gView->renderView($module['gear']);
      $out .= <<<END
        <h5 class="pull-right"><em>Contributed by {$module['user']['name_first']} {$module['user']['name_last']}</em></h5>
      </div>
    </div>
  </div>
END;
      return $out;
		}
		
    public function renderAddModule($user_id, $dt_token){
			$out = <<<END
<div class="col-lg-6 col-md-6 col-xs-12 grid-item">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title row">
        <div class="col-sm-12">
          <a>Create New Module</a>
        </div>
      </h4>
    </div>
    <div class="panel-body">
      <form class='form module-form' role="form" action='javascript:dt.post({"url":
END;
      $out .= '"'. DTSettingsConfig::baseURL("consumers/secure/module_consumer.php") .'"';
      $out .= ',"success":function(){window.location.reload()}';
      $out .= <<<END
,"form":".module-form"})'>
        <input type="hidden" name="user_id" value="{$user_id}" />
        <input type="hidden" name="act" value="addmodule" />
			  <input type="hidden" name="tok" value="{$dt_token}" />
        <div class="form-group">
      		<input type="text" placeholder="Name" class="form-control" name="name" required>
      	</div>
      	<div class="form-group">
      		<select class="form-control" name="module_category_id" required>
        		<option value="" disabled selected>Select a Category</option>
END;
              $mcConsumer = new DTProviderConsumer("gear","modulecategory.php",$dt_token);
              $o = $mcConsumer->request("list",array());
              foreach($o['items'] as $obj){
              	$out .= "<option value='{$obj['id']}'>".$obj['name']."</option>";
              };
              
      $out .= <<<END
      		</select>
      	</div>
      	<input type='hidden' name='type' value='create_module'>
        <input type="submit" class='btn btn-green pull-right' value="Save" />
      </form>
    </div>
  </div>
</div>
END;
      return $out;
		}
		
	}