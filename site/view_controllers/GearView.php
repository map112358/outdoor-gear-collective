<?php
	class GearView extends ViewController {
		public function renderRow($g){
			$str = "";
			$random = rand(0,2000);
			$formated_weight = number_format($g['weight'],2);
			$brand_link = DTSettingsConfig::baseURL("gear/list.php?brand_id={$g['brand']['id']}");
      return <<<END
      <tr>
        <td>{$g['brand']['name']}</td>
        <td>{$g['name']}</td>
        <td class='weight'>{$formated_weight}</td>
      </tr>
END;
    }
		public function renderModalRow($gear,$moduleOrPack){
			$str = "";
			$random = rand(0,2000);
			$formated_weight = number_format($gear['weight'],2);
			$brand_link = DTSettingsConfig::baseURL("gear/list.php?brand_id={$gear['brand']['id']}");
      $out = <<<END
      <tr>
        <td>
END;
        $dt_token = DTAPI::consumerTokenForAPI("gear");
        $hasGear = false;
        //DTLog::debug($module['gear']);
        foreach($moduleOrPack['gear'] AS $g) {
          if($g['id']==$gear['id']){
            $hasGear = true;
          }
        }
        
        $buttonStart = '<span';
        $idAttr = 'id="gear'.$gear['id'].'"';
        $buttonEnd = "></span>";
        $consumerUrl = DTSettingsConfig::baseURL("consumers/secure/module_consumer.php");
        $removeFunction = "removeGearFromModule";
        $addFunction = "addGearToModule";
        if(isset($moduleOrPack['modules'])){
          $consumerUrl = DTSettingsConfig::baseURL("consumers/secure/pack_consumer.php");
          
          $idAttr = ' id="gearPack'.$gear['id'].'"';
          $removeFunction = "removeGearFromPack";
          $addFunction = "addGearToPack";
        }
        
        $out .= $buttonStart;
        if($hasGear) {
          $class = "class='fa fa-check green padding-right-xs pointer'".$idAttr;
          $out .= ' '.$class.' onclick="'.$removeFunction."('".$consumerUrl."','".$dt_token."',".$gear['id'].",".$moduleOrPack['id'].')"';
        } else {
          $class = "class='fa fa-plus blue padding-right-xs pointer'".$idAttr;
          $out .= " ".$class." onclick=\"".$addFunction."('".$consumerUrl."','".$dt_token."',".$gear['id'].",".$moduleOrPack['id'].')"';
        }
        $out .= $buttonEnd;
        $out .= <<<END
            </td>
        <td>{$gear['brand']['name']}</td>
        <td>{$gear['name']}</td>
        <td class='weight'>{$formated_weight}</td>
      </tr>
END;
      return $out;
    }
    
    function renderAddGearPanel() {
            
      $out = <<<END
      <h3>Missing your favorite gear?</h3>
      <p>Please be responsible when adding new gear. We hope you can provide us with the most accurate information possible, and try to prevent the creation of duplicate gear on our site.  Please report any inaccuracies you find on GearCrossing. Thanks for contributing!</p>
      <form class='form create-gear-form' role="form" action="javascript:dt.post({'url':
END;
      $out .= "'". DTSettingsConfig::baseURL("consumers/secure/gear_consumer.php") ."'";
      $dt_token = DTAPI::consumerTokenForAPI("gear");
      $out .= ",'success':function(){window.location.reload()}";
      $out .= <<<END
,'form':'.create-gear-form'})">
        <input type="hidden" name="act" value="addgear" />
			  <input type="hidden" name="tok" value="{$dt_token}" />
        <div class="form-group">
      		<input type="text" placeholder="Name" class="form-control" name="name" required>
      	</div>
      	<div class="form-group">
      		<input type="number" placeholder="weight (oz)" class="form-control" name="weight" step="0.1" required>
      	</div>
        <div class="form-group">
        	<select class="form-control" name="brand_id" required>
          	<option value="" disabled selected>Brand</option>
END;
        		
      $bConsumer = new DTProviderConsumer("gear","brand.php",$dt_token);
      $o = $bConsumer->request("list",array("count"=>1000));
      foreach($o['items'] as $obj){
      	$out .= "<option value='{$obj['id']}'>".$obj['name']."</option>";
      };
            
      $out .= <<<END
        	</select>
        </div>
        <div class="form-group">
        	<select class="form-control" name="category_id" required>
          	  <option value="" disabled selected>Category</option>
END;
      $mcConsumer = new DTProviderConsumer("gear","modulecategory.php",$dt_token);
      $o = $mcConsumer->request("list",array());
      foreach($o['items'] as $obj){
      	$out .= "<option value='{$obj['id']}'>".$obj['name']."</option>";
      };
    
      $out .= <<<END
        	</select>
        </div>
    
      	<input type='hidden' name='type' value='create_gear'>
        <input class="btn btn-green" type="submit" name="submit" value="Submit">
      </form>
END;
      return $out;
    }
	}