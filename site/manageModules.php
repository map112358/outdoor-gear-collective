<?php
session_start();
if($_SERVER["HTTPS"] != "on") {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

if(!isset($_SESSION['username']) ) {
  header("Location: login.php");
}
require_once dirname(__FILE__).'/../gear.inc.php';
$user = new Users($db->filter(array("username"=>$_SESSION['username'])));


function getGearJSON($gear) {
  $json = "[";
  foreach($gear AS $g) {
    $json .= '{"id":"'.$g['id'].'", "name":"'.$g['name'].'", "weight":'.$g['weight'].', "brand":"'.$g['brand']['name'].'"},';
  }
  if(count($gear) > 0) {
    $json = substr($json, 0, -1);
  }
  $json .= "]";
  return $json;
}

function getModuleJSON($module) {
  $json = "[";
  foreach($module AS $m) {
    $json .= '{"name":"'.$m['name'].'", "id":"'.$m['id'].'", "category":"'.$m['category']['name'].'", "gear":';
    $json .= getGearJSON($m['gear']);
    $json .= '},';
  }
  if(count($module) > 0) {
    $json = substr($json, 0, -1);
  }
  $json .= "]";
  return $json;
}

function getModuleCategoryJSON($category) {
  $json = "[";
  foreach($category AS $m) {
    $json .= '{"id":"'.$m['id'].'", "name":"'.$m['name'].'","modules":';
    $json .= getModuleJSON($m['modules']);
    $json .= '},';
  }
  if(count($category) > 0) {
    $json = substr($json, 0, -1);
  }
  $json .= "]";
  return $json;
}

if($params['type']=='create_link') {
  try{
    $params1 = array("module_id"=>$params['module_id'],"gear_id"=>$params['gear_id']);
    $g = GearModule::upsert($db->filter($params1),$params1);
  
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
  echo "success";
} else if($params['type']=='create_link_module_pack') {
  try{
    $params1 = array("pack_id"=>$params['pack_id'],"module_id"=>$params['module_id']);
    $g = PackModule::upsert($db->filter($params1),$params1);
    $db->commit();
    header("Location: pack.php?id=".$g['pack_id']);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
} else if($params['type']=='create_link_module_gear') {
  try{
    $module = new Module($db->filter(array("user_id"=>$user['id'],"id"=>$params['module_id'])));
    $params1 = array("gear_id"=>$params['gear_id'],"module_id"=>$module['id']);
    $g = GearModule::upsert($db->filter($params1),$params1);
    $db->commit();
    header("Location: module.php?id=".$g['module_id']);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
}  else if($params['type']=='create_module') {
  try{
    $params1 = array("name"=>$params['name'],"module_category_id"=>$params['module_category_id'],"user_id"=>$user['id']);
    $g = Module::upsert($db->filter($params1),$params1);
    $db->commit();
    header("Location: module.php?id=".$g['id']);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
} else if($params['type']=='gear_category') {
  try{
    $params1 = array("id"=>$params['gear_id'],"category_id"=>$params['category_id']);
    $g = Gear::upsert($db->filter(array("id"=>$params['gear_id'])),$params1);
  
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
  echo "success";
} else if ($params['type']=='create_category') {
  try{
    $params1 = array("name"=>$params['name']);
    $g = ModuleCategory::upsert($db->filter($params1),$params1);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
  header("Location: newGear.php");
} else if ($params['type']=='create_brand') {
  try{
    $params1 = array("name"=>$params['name'],"");
    $g = Brand::upsert($db->filter($params1),$params1);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
  header("Location: newGear.php");
} else if ($params['type']=='create_gear') {
  try{
    $params1 = array("name"=>$params['name'],"weight"=>$params['weight'],"category_id"=>$params['category_id'],"brand_id"=>$params['brand_id']);//,"user_id"=>$user['id']);
    $g = Gear::upsert($db->filter($params1),$params1);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
  header("Location: newGear.php");
} else if ($params['type']=='create_pack') {
  try{
    $params1 = array("name"=>$params['name'],"user_id"=>$user['id']);
    $params2 = array("name"=>$params['name'],"user_id"=>$user['id'],"description"=>$params['description'],"is_public"=>"false");
    //$params2 = array("name"=>$params['name'],"user_id"=>$user['id'],"is_public"=>true);
    $g = Pack::upsert($db->filter($params1),$params2);
    $db->commit();
    header("Location: pack.php?id=".$g['id']);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
} else if ($params['type']=='get_category') {
  try{
    $category = ModuleCategory::select($db->filter(array()));
    if(isset($params['id'])) {
      $params1 = array("id"=>$params['id']);
      $category = ModuleCategory::select($db->filter($params1));
    }
    $json = getModuleCategoryJSON($category);
    echo $json;
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
} else if ($params['type']=='get_pack') {
  try{
    $pack = Pack::select($db->filter(array()));
    if(isset($params['id'])) {
      $params1 = array("id"=>$params['id']);
      $pack = Pack::select($db->filter($params1));
    }
    $json = getModuleCategoryJSON($pack);
    echo $json;
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
} else if ($params['type']=='get_module') {
  try{
    $module = Module::select($db->filter(array()));
    if(isset($params['id'])) {
      $params1 = array("id"=>$params['id']);
      $module = Module::select($db->filter($params1));
    }
    $json = getModuleJSON($module);
    echo $json;
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
} else if ($params['type']=='get_gear') {
  try{
    $gear = Gear::select($db->filter(array()));
    if(isset($params['id'])) {
      $params1 = array("id"=>$params['id']);
      $gear = Gear::select($db->filter($params1));
    }
    $json = getGearJSON($gear);
    echo $json;
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
} else if ($params['type']=='del_module_gear') {
  try{
    $gm = new GearModule($db->filter(array("module_id"=>$params["module_id"],"gear_id"=>$params["gear_id"])));
    $gm->delete();
    $db->commit();
    header("Location: module.php?id=".$params["module_id"]);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
  $db->commit();
} else if ($params['type']=='del_pack_module') {
  try{
    $pm = new PackModule($db->filter(array("module_id"=>$params["module_id"],"pack_id"=>$params["pack_id"])));
    $pm->delete();
    $db->commit();
    header("Location: pack.php?id=".$params["pack_id"]);
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params); }
} else if ($params['type']=='del_module') {
  try{
    $m = new Module($db->filter(array("id"=>$params["module_id"])));
    $m->delete();
    $db->commit();
    header("Location: module.php");
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params->allParams()); }
} else if ($params['type']=='del_pack') {
  try{
    $m = new Pack($db->filter(array("id"=>$params["pack_id"],"user_id"=>$user['id'])));
    $m->delete();
    $db->commit();
    header("Location: pack.php");
  }catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$params->allParams()); }
}