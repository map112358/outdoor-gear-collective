<?php
include_once("prepage.php");  
?>
<h3>Modules</h3>

<h4>Get module categories.</h4>
<?php


$moduleCategories = ModuleCategory::select($db->filter()->orderby("name ASC"));
foreach($moduleCategories AS $m) {
  echo $m['name']."<br>";
}

$kitchenGear = 
?>



<h4>Select a module and a gear item to add.</h4>


  <select class='module_id'>
<?php
foreach($moduleCategories AS $mC) {
  foreach($mC['modules'] AS $m){
    echo "<option value='{$m['id']}'>{$mC['name']} {$m['name']}</option>";
  }
}?>
  </select>
  <select class='gear_id'>
  <?php
    $gear = Gear::select($db->filter()->orderby("weight DESC"));
    foreach($gear AS $g)
      echo "<option value='{$g['id']}'>{$g['brand']['name']} {$g['name']}</option>";
  ?>
  </select>
  <button name="add" onclick="addGearModule()">Save</button>

<script>
addGearModule = function() {
  $.post("manageModules.php", {module_id: $('.module_id option:selected').val(), gear_id: $('.gear_id option:selected').val()}).done(function(data){console.log(data)})
}
</script>
<?php
include_once("postpage.php")
?>