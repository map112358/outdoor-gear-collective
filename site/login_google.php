<?php
require_once dirname(__FILE__)."/site.inc.php";
//require($prepage);
error_reporting(E_ALL);
$session = DTSession::sharedSession();
$dt_token = DTAPI::consumerTokenForAPI("gear");

//make sure we have a request token to authorize
if(!isset($session["gear_oauth_request_token"])){ // we need to have this first...
	header("Location: index.php");
	exit();
}

$tok=DTAPI::consumerTokenForAPI("google");
$consumer = new GooglePlusConsumer("google","",$tok);
$profile = $consumer->request("userinfo");

if(isset($profile,$profile["id"])){
	setcookie("login_method","google",time()+60*60*24*3000);
	$consumer = new DTProviderConsumer("gear","authentication.php",$dt_token);
	$consumer->request("authorize_google_plus",array("gp_acc"=>$session["google_oauth_access_token"],"req_tok"=>$session["gear_oauth_request_token"],"gpid"=>$profile["id"]));
	// we should be redirected to the DT verifier at this point
}

?>

<script>
history.back();
</script>

<?php
//require($postpage);
