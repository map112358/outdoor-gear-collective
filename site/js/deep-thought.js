var dt = {};
dt.post = function(args){
	args.type = "POST";
	dt.request(args);
}

dt.get = function(args){
	args.type = "GET";
	dt.request(args);
}

dt.request = function(args){
	params = $(args.form).serializeObject();
	data = (typeof args.data === "undefined" || args.data==null ?{}:args.data);
	$.each(data,function(idx,item){
		params[idx]=item;
	});
	$.each(params,function(idx,item){
		if(item && item.push)
			params[idx] = JSON.stringify(item);
	});
	
	if(typeof args.success === "undefined") //default behavior is to return to referrer
		args.success = function(){ window.location.href=document.referrer; };

	$.ajax({
	    type: args.type,
	    url: args.url,
	    data: params,
	    dataType: "jsonp",
	    statusCode:{
	    	/*278: function (data){ //custom client redirect code
	    		window.location.replace(data.obj.location);
	    	},*/
	    	500: function (data){
	    		console.log("Request failed: "+data);
	    	}
	    },
	    success: function(data,responseText){
  	    //console.log("SUCCESS IN DT");
	    	if( data.err == 278 ){ //Firefox seems oblivious to our 278 HTTP status code...
			    window.location.replace(data.obj.location);
			  } else if(args!=null && typeof args.error !== "undefined" && data.err>0){
	    		args.error(data.err,data.obj);
	    	}else if(args!=null && typeof args.success !== "undefined"){
		    	args.success(data.obj);
		    }
	    },
	    error: function(xhr,status,error) {
  	    if(args!=null && typeof args.error !== "undefined"){
		    	args.error(data.obj);
		    } else {
    	    console.log("ERROR OCCURRED");
    	    console.log("STATUS: "+status);
    	    console.log("ERROR: "+error);
  	    }
	    },
	    complete: function(){
  	    
	    }
	});
}

//** allows file uploads -- requires jquery.iframe-transport.js */
dt.upload = function(args){
	params = $(args.form).serializeObject();
	data = (typeof args.data === "undefined" || args.data==null ?{}:args.data);
	$.each(data,function(idx,item){
		params[idx]=item;
	});
	$.each(params,function(idx,item){
		if(item.push)
			params[idx] = JSON.stringify(item);
	});
	
	if(typeof args.success === "undefined") //default behavior is to return to referrer
		args.success = function(){ window.location.href=document.referrer; };
		
	$.ajax({
	    type: "POST",
	    url: args.url,
	    data: params,
	    dataType: "jsonp",
	    statusCode:{
	    	278: function (data){ //custom client redirect code
	    		window.location.replace(data.obj.location);
	    	}
	    },
	    success: function(data,responseText){
  	    console.log("In Successfunction")
	    	if(args!=null && typeof args.error !== "undefined" && data.err>0){
  	    	window.location.reload();
	    		args.error(data.err,data.obj);
	    	}else if(args!=null && typeof args.success !== "undefined"){
  	    	window.location.reload();
		    	args.success(data.obj);
		    }
		    window.location.reload();
	    },
	    error: function(xhr,status,error) {
  	    if(args!=null && typeof args.error !== "undefined"){
		    	args.error(data.obj);
		    } else {
    	    console.log("ERROR OCCURRED");
    	    console.log("STATUS: "+status);
    	    console.log("ERROR: "+error);
  	    }
	    },
	    files: $(":file"),
		iframe: true,
		processData: false
	});
}

$.fn.serializeObject = function(){
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};