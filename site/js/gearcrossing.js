$(document).ready(function() { 
  var $grid = $('.grid').masonry({
    // options
    itemSelector: '.grid-item'
  });
})
function resizeTextAreaEvent() {
  var $textareas = jQuery('textarea');
  
  // set init (default) state   
  $textareas.data('x', $textareas.outerWidth());
  $textareas.data('y', $textareas.outerHeight());
  
  $textareas.mouseup(function () {
    var $this = $(this);
  
    if ($this.outerWidth() != $this.data('x') || $this.outerHeight() != $this.data('y')) {
      fresh();
    }
  
    // set new height/width
    $this.data('x', $this.outerWidth());
    $this.data('y', $this.outerHeight());
  });
}

function calculateWeights(){
  $('.table-sum').each(function(){
    sum = 0;
    $(this).find('.weight').each(function(){
      if($(this).siblings('.carry').length < 1 | $(this).siblings('.carry').find('.checkbox').first().prop('checked') == true) {
        sum += Number($(this).text())
      }
    });
    $(this).find('.total').each(function(){
      $(this).text(sum.toFixed(2))
    })
  });
  
  $('.pack-container').each(function(){
    sum = 0;
    $(this).find('.total').each(function(){
      sum += Number($(this).text())
    })
    $(this).find('.grand-total').text(sum.toFixed(2) +" oz ("+(sum/16).toFixed(2)+" lbs)")
  })
}

function fresh() {
  calculateWeights();
  $grid = $('.grid').masonry({
    // options
    itemSelector: '.grid-item'
  });
}

function appendGrid() {
  
}

//---------------------------------
//----DT interaction---------------
//---------------------------------

//pack
function deletePack(url, tok, packID) {
  if (window.confirm("Delete this pack forever?")) {
    var data = {"act":"deletePack", "tok":tok, "pack_id":packID};
    dt.post({"url":url,"data":data,"success":function(){window.location.reload();}})
  }
}

//pack
function deleteModule(url, tok, moduleID) {
  if (window.confirm("Delete this module forever?")) {
    var data = {"act":"deleteModule", "tok":tok, "module_id":moduleID};
    dt.post({"url":url,"data":data,"success":function(){window.location.reload();}})
  }
}

//gearModule
function addGearToModule(url, tok, gearID, moduleID) {
  var data = {"act":"addGearToModule", "tok":tok, "gear_id":gearID, "module_id":moduleID};
  dt.post({"url":url,"data":data,"success":function(){
    toggleGearModuleAdded(url, tok, gearID, moduleID);
  }})
}

function removeGearFromModule(url, tok, gearID, moduleID) {
  var data = {"act":"removeGearFromModule", "tok":tok, "gear_id":gearID, "module_id":moduleID};
  dt.post({"url":url,"data":data,"success":function(){
    toggleGearModuleRemoved(url, tok, gearID, moduleID);
  }})
}

//search Models
function searchUserModels(url, tok, term, domSelector, userid) {
  var data = {"act":"search", "tok":tok, "like":term, "user_id":userid };
  dt.post({
    "url":url,
    "data":data,
    "success":function(html){
      $(domSelector).append(html);
      fresh();
    }
  })
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

//search Models
function searchModels(url, tok, term, domSelector) {
  
  if(term == ""){//no delay applied
    var data = {"act":"search", "tok":tok, "like":term};
    dt.post({
      "url":url,
      "data":data,
      "success":function(html){
        page = 2;
        gpage = 2;
        mpage = 2;
        $(domSelector).empty();
        $(domSelector).append(html);
        fresh();
      }
    });
  } else {//apply a delay, so we don't search all prefixes (i.e. h, he, hel, hell, hello)
    delay(function(){
      var data = {"act":"search", "tok":tok, "like":term};
      dt.post({
        "url":url,
        "data":data,
        "success":function(html){
          page = 2;
          gpage = 2;
          mpage = 2;
          $(domSelector).empty();
          $(domSelector).append(html);
          fresh();
        }
      });
    },400);
  }
}

//search Models
function searchGearPackModals(url, tok, packID, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  if(like == ""){//no delay applied
    var data = {"act":"search", "tok":tok, "like":like, "ismodal":true, "pack_id":packID};
    dt.post({
      "url":url,
      "data":data,
      "success":function(html){
        page = 2;
        gpage = 2;
        mpage = 2;
        $(domSelector).empty();
        $(domSelector).append(html);
        fresh();
      }
    });
  } else {//apply a delay, so we don't search all prefixes (i.e. h, he, hel, hell, hello)
    delay(function(){
      var data = {"act":"search", "tok":tok, "like":like, "ismodal":true, "pack_id":packID};
      dt.post({
        "url":url,
        "data":data,
        "success":function(html){
          page = 2;
          gpage = 2;
          mpage = 2;
          $(domSelector).empty();
          $(domSelector).append(html);
          fresh();
        }
      });
    },400);
  }
}

//search Models
function searchGearModuleModals(url, tok, moduleID, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  if(like == ""){//no delay applied
    var data = {"act":"search", "tok":tok, "like":like, "ismodal":true, "module_id":moduleID};
    dt.post({
      "url":url,
      "data":data,
      "success":function(html){
        page = 2;
        gpage = 2;
        mpage = 2;
        $(domSelector).empty();
        $(domSelector).append(html);
        fresh();
      }
    });
  } else {//apply a delay, so we don't search all prefixes (i.e. h, he, hel, hell, hello)
    delay(function(){
      var data = {"act":"search", "tok":tok, "like":like, "ismodal":true, "module_id":moduleID};
      dt.post({
        "url":url,
        "data":data,
        "success":function(html){
          page = 2;
          gpage = 2;
          mpage = 2;
          $(domSelector).empty();
          $(domSelector).append(html);
          fresh();
        }
      });
    },400);
  }
}

//search Models
function searchModulePackModals(url, tok, packID, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  if(like == ""){//no delay applied
    var data = {"act":"search", "tok":tok, "like":like, "ismodal":true, "pack_id":packID};
    dt.post({
      "url":url,
      "data":data,
      "success":function(html){
        page = 2;
        gpage = 2;
        mpage = 2;
        $(domSelector).empty();
        $(domSelector).append(html);
        calculateWeights();
      }
    });
  } else {//apply a delay, so we don't search all prefixes (i.e. h, he, hel, hell, hello)
    delay(function(){
      var data = {"act":"search", "tok":tok, "like":like, "ismodal":true, "pack_id":packID};
      dt.post({
        "url":url,
        "data":data,
        "success":function(html){
          page = 2;
          gpage = 2;
          mpage = 2;
          $(domSelector).empty();
          $(domSelector).append(html);
          calculateWeights();
        }
      });
    },400);
  }
}

//gearPack
function addGearToPack(url, tok, gearID, packID) {
  var data = {"act":"addGearToPack", "tok":tok, "gear_id":gearID, "pack_id":packID};
  dt.post({"url":url,"data":data,"success":function(){toggleGearPackAdded(url, tok, gearID, packID);}})
}

function removeGearFromPack(url, tok, gearID, packID) {
  var data = {"act":"removeGearFromPack", "tok":tok, "gear_id":gearID, "pack_id":packID};
  dt.post({"url":url,"data":data,"success":function(){toggleGearPackRemoved(url, tok, gearID, packID);}})
}

//packModule
function addModuleToPack(url, tok, moduleID, packID) {
  var data = {"act":"addModuleToPack", "tok":tok, "module_id":moduleID, "pack_id":packID};
  dt.post({"url":url,"data":data,"success":function(){toggleModuleAdded(url, tok, moduleID, packID);}})
}

function removeModuleFromPack(url, tok, moduleID, packID) {
  var data = {"act":"removeModuleFromPack", "tok":tok, "module_id":moduleID, "pack_id":packID};
  dt.post({"url":url,"data":data,"success":function(){toggleModuleRemoved(url, tok, moduleID, packID);}})
}

function removeModuleFromPackNoToggle(url, tok, moduleID, packID) {
  var data = {"act":"removeModuleFromPack", "tok":tok, "module_id":moduleID, "pack_id":packID};
  dt.post({"url":url,"data":data,"success":function(){window.location.reload();}})
}

function toggleModuleAdded(url, tok, moduleID, packID) {
  $('#module'+moduleID).removeClass("blue").removeClass("fa-plus").addClass("green").addClass("fa-check").attr("onclick","removeModuleFromPack('"+ url +"','"+ tok +"',"+ moduleID +","+ packID +")")
}

function toggleModuleRemoved(url, tok, moduleID, packID) {
  $('#module'+moduleID).removeClass("green").removeClass("fa-check").addClass("fa-plus").attr("onclick","addModuleToPack('"+ url +"','"+ tok +"',"+ moduleID +","+ packID +")")
}

function toggleGearModuleAdded(url, tok, gearID, moduleID) {
  $('#gear'+gearID).removeClass("blue").removeClass("fa-plus").addClass("green").addClass("fa-check").attr("onclick","removeGearFromModule('"+ url +"','"+ tok +"',"+ gearID +","+ moduleID +")")
}

function toggleGearModuleRemoved(url, tok, gearID, moduleID) {
  $('#gear'+gearID).removeClass("green").removeClass("fa-check").addClass("blue").addClass("fa-plus").attr("onclick","addGearToModule('"+ url +"','"+ tok +"',"+ gearID +","+ moduleID +")")
}

function toggleGearPackAdded(url, tok, gearID, packID) {
  $('#gearPack'+gearID).removeClass("blue").removeClass("fa-plus").addClass("green").addClass("fa-check").attr("onclick","removeGearFromPack('"+ url +"','"+ tok +"',"+ gearID +","+ packID +")")
}

function toggleGearPackRemoved(url, tok, gearID, packID) {
  $('#gearPack'+gearID).removeClass("green").removeClass("fa-check").addClass("blue").addClass("fa-plus").attr("onclick","addGearToPack('"+ url +"','"+ tok +"',"+ gearID +","+ packID +")")
}


//infinite scroll
function populateGearModal(url, tok, page, moduleID, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  var data = {"act":"list", "tok":tok, "page":page, "module_id":moduleID, "like":like};
  dt.post({"url":url,"data":data,"success":function(html){
    $(domSelector).append(html);
    calculateWeights();
  }});
}

function populateGearPackModal(url, tok, page, packID, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  var data = {"act":"list", "tok":tok, "page":page, "pack_id":packID, "like":like};
  dt.post({"url":url,"data":data,"success":function(html){
    $(domSelector).append(html);
    calculateWeights();
  }});
}

function populateModulePackModal(url, tok, page, packID, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  var data = {"act":"list", "ismodal":true, "tok":tok, "page":page, "pack_id":packID, "like":like};
  dt.post({"url":url,"data":data,"success":function(html){
    $(domSelector).append(html);
    calculateWeights();
  }});
}

function populatePage(url, tok, page, domSelector, like) {
  if(typeof(like)==="undefined") like = ""
  var data = {"act":"list", "tok":tok, "page":page, "like":like};
  dt.post({
    "url":url,
    "data":data,
    "success":function(html){
      if(url.indexOf('gear/api') !=-1){
        console.log("found gear_consumer");
        $(domSelector).append(html);
        fresh(); 
      } else {
        $(domSelector).append(html).masonry( 'appended', $('.page'+page) );
        fresh(); 
      }
    }
  })
}





