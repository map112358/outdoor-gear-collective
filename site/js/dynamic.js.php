<?php
	if(!isset($stats)){
		$stats = array(
			"unused"=>0,
			"used"=>0,
			"quota"=>1
		);
	}
?>
<script>
function fetch(consumer,action,successf,data,load_sel){
	if(data==null)
		data = {}
	data["act"] = action;
	data["tok"] = "<?=$dt_token?>";
	if(load_sel!=null)
		$(load_sel).html('<i class="fa fa-refresh fa-spin"></i>');
	dt.post({
		url:consumer,
		data:data,
		success: successf
	});
}

</script>