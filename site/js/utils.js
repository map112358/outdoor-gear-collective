function addCommas(x) {
	if(x==null)
		return 0;
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function reduceNotation(x){
	if(x==0)
		return "-";
	else if(Math.abs(x)<1000)
		return x;
	else if(Math.abs(x)>=1000 && Math.abs(x)<1000000)
		return (x/1000).toFixed(3)+"K";
	else if(Math.abs(x)>=1000000 && Math.abs(x)<1000000000)
		return (x/1000000).toFixed(3)+"M";
		
	return (x/1000000000).toFixed(3)+"B";
}

function USD(val){
	return "<sup>$</sup>"+addCommas(val.toFixed(2)).replace(/\.(.*)/, "<sup>.$1</sup>");
}

function populateSelect(list,sel,defaults){
	oldvals = $(sel).select2("val");
	if((oldvals==null || oldvals.length==0) && defaults!=null)
		oldvals = defaults;
	html = "<option value=''></option>";
	$.each(list,function(idx,item){
		html += "<option value='"+item.value+"'>"+item.label+"</option>";
	});
	$(sel).html(html);
    $(sel).select2("val",oldvals);
	$(sel).siblings().filter('.loading').html("");
}

function populateTags(list,sel,defaults){
	tags = [];
	$.each(list,function(idx,item){
		tags.push(item.label);
	});
	$(sel).select2({"tags":tags,allowClear:true});
	
	oldvals = $(sel).select2("val");
	if((oldvals==null || oldvals.length==0) && defaults!=null)
		oldvals = defaults;
    $(sel).select2("val",oldvals);
	$(sel).siblings().filter('.loading').html("");
}

function populateTag(list,sel,defaults){
	tags = [];
	$.each(list,function(idx,item){
		tags.push(item.label);
	});
	$(sel).select2({"tags":tags,allowClear:true,maximumSelectionSize:1});
	
	oldvals = $(sel).select2("val");
	if((oldvals==null || oldvals.length==0) && defaults!=null)
		oldvals = defaults;
    $(sel).select2("val",oldvals);
	$(sel).siblings().filter('.loading').html("");
}

function drawPieChart(title,data,element_id,options){
	if(typeof(options)==="undefined")
		options = { title: title, sliceVisibilityThreshold: 0, colors: google_chart_colors, backgroundColor:'transparent' };
	var chart = new google.visualization.PieChart(document.getElementById(element_id));
	chart.draw(data, options);
}

function drawBarChart(title,data,element_id,options){
	if(typeof(options)==="undefined")
		options = {};
	options.title = title;
	options.colors=google_chart_colors;
	options.backgroundColor='transparent';
	var chart = new google.visualization.BarChart(document.getElementById(element_id));
	chart.draw(data, options);
}

function drawLineChart(title,data,element_id,options){
	if(typeof(options)==="undefined")
		options = { title: title, colors: google_chart_colors, backgroundColor:'transparent' };
	var chart = new google.visualization.LineChart(document.getElementById(element_id));
	chart.draw(data, options);
}

function drawTimeline(title,data,element_id,options){
	if(typeof(options)==="undefined")
		options = {};

	options.title=title;
	options.colors=google_chart_colors;
	options.cluster=true;
	options.axisOnTop=true;
	options.zoomMax=365*24*60*60*1000;
	options.zoomMin=7*24*60*60*1000;
	
    var timeline = new links.Timeline(document.getElementById(element_id),options);
    timeline.draw(data);
    return timeline;
}

function array_chunk(arr,l){
    var out = [];
    for (var i=0; i<arr.length; i+=l)
    	out.push(arr.slice(i,i+l));
    return out;
}