<?php
	require_once dirname(__FILE__)."/../../gear.inc.php";
	$params = $_REQUEST;
	if(isset($params["act"]) && $params["act"]=="update"){
		if(sizeof($_FILES)>0 && !empty($_FILES["profile-image"]["tmp_name"])){
			$path_parts = pathinfo($_FILES['profile-image']['name']);
			$params["file_ext"] = $path_parts["extension"];
			$params["profile_pic"] = true;
			if(!in_array($params["file_ext"], array("pdf","gif","jpg","bmp","jpeg","png"))){
				DTLog::error("Bad file extension ({$params['file_ext']})!");
				exit(); 
			}
		} else if(sizeof($_FILES)>0 && !empty($_FILES["banner-image"]["tmp_name"])){
  		DTLog::debug("In banner image before");
			$path_parts = pathinfo($_FILES['banner-image']['name']);
			$params["file_ext"] = $path_parts["extension"];
			$params["banner_pic"] = true;
			if(!in_array($params["file_ext"], array("pdf","gif","jpg","bmp","jpeg","png"))){
				DTLog::error("Bad file extension ({$params['file_ext']})!");
				exit(); 
			}
		}
	}
	
	$consumer = new DTSecureProviderConsumer("gear","secure/users.php");
	$response = $consumer->requestAndRespond($params);
	
	if($params["act"]=="update"){ //we have some post-processing to do
		$o = $response->obj;
		if(isset($o)){
			if(isset($o["image"]) && sizeof($_FILES)!=0){
  			if(sizeof($_FILES)>0 && !empty($_FILES["banner-image"]["tmp_name"])){
          DTLog::debug("In banner image after");
				  $fname = DTFile::upload(dirname(__FILE__).'/../../uploads/',$o['image_header']['id'],"banner-image");
        } else if(sizeof($_FILES)>0 && !empty($_FILES["profile-image"]["tmp_name"])){
          DTLog::debug("In profile image");
				  $fname = DTFile::upload(dirname(__FILE__).'/../../uploads/',$o['image']['id'],"profile-image");
        }
		  }
		  header("Location: ".DTSettingsConfig::baseURL("users/profile.php?id=".$o['id']));
    } else
      DTLog::debug("o is not set");
  }