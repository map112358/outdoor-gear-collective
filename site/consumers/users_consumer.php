<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	$params = $_REQUEST;
	$consumer = new DTProviderConsumer("gear","users.php");
  
  if(isset($params["act"]) && $params["act"]=="register"){ //we have some post-processing to do
  	$response = $consumer->requestAndRespond($params);
		$obj = $response->obj;
		if(isset($obj)){
			//check for successful registration and send email
			$alias = $params["alias"];
		
			//get a reset token
			$req_tok = DTAPI::consumerTokenForAPI("gear");
			$token_consumer = new DTProviderConsumer("gear","authentication.php",$req_tok);
			$provider_token = $token_consumer->upgradeToken($params["tok"]);
			$reset_token = $token_consumer->request("password_reset_token",array("email"=>$obj["email"]),$provider_token);
			$token = $reset_token["token"];
			
			$reset_link = DTSettingsConfig::baseURL("users/set_password.php?alias=".urlencode($alias)."&rst={$token}");
				
			$mailer = new DTPHPMailer(null,null,dirname(__FILE__)."/../");
			$to = new DTEmailRecipientList();
			$to->setAddresses(array("email"=>$obj["email"],"name"=>$obj["name"]));
			$bcc = new DTEmailRecipientList();
			$bcc->setAddresses(array("email"=>"blake@expressiveanalytics.com")); //we're gonna watch these for a while...
			$mailer->bcc = $bcc;
			$email_reset_url = $reset_link;
			$email_name = $obj["name"];
			ob_start(); //capture the following as HTML
			require_once(dirname(__FILE__)."/../email/register.php"); //registration template
			$html = ob_get_contents();
			ob_end_clean();
			$mailer->sendHTMLEmail($to,"Welcome to gear.com!",$html);
		}
	}	else if(isset($params["act"]) && $params["act"]=="search"){ //we have some post-processing to do
  	$response = new DTResponse($consumer->request($params["act"],$params));
    $hView = new VCList(new DTParams());
    $hView->setItems($response->obj['items']);
    $out = '<div class="grid">';
    $out .= $hView->renderEach( function($obj){
    	$profileView = new UserProfileView();
      return $profileView->renderUserCard($obj);
    });
    $out .= '</div>';
    $response->obj = $out;
    $response->respond($params);
		return $response;
	} else {
  	$response = $consumer->requestAndRespond($params);
	}