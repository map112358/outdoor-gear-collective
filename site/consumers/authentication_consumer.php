<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	$params = $_REQUEST;
	$consumer = new DTProviderConsumer("gear","authentication.php");
	$response = $consumer->requestAndRespond($params);
	
	if($params["act"]=="password_reset_token"){
		$obj=$response->obj;
		$email = $obj["email"]; //this is actually our email address
		if(empty($email)) 
			exit(); // we don't have a valid email address here...
		$token = $obj["token"];
		$reset_link = DTSettingsConfig::baseURL("users/set_password.php")."?email=".urlencode($params["email"])."&rst={$token}";
		
		$mailer = new DTPHPMailer(null,null,dirname(__FILE__)."/../../");
		$to = new DTEmailRecipientList();
		$to->setAddresses($email);
		$email_reset_url = $reset_link;
		
		ob_start(); //capture the following as HTML
		require_once(dirname(__FILE__)."/../../email/recovery.php"); //registration template
		$html = ob_get_contents();
		ob_end_clean();
		$mailer->sendHTMLEmail($to,"GearCrossing.com Account Recovery",$html);
	}