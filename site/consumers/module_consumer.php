<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	$params = $_REQUEST;
  $dt_token = DTAPI::consumerTokenForAPI("gear");
	$consumer = new DTProviderConsumer("gear","module.php",$dt_token);
	
  if($params["act"]=="search"){ //we have some post-processing to do
  	$response = new DTResponse($consumer->request($params["act"],$params));
    $mView = new VCList(new DTParams());
    $mView->setItems($response->obj['items']);
    $out = "";
    if(isset($params["pack_id"])) {//request for pack's modules
      $pack_id = $params["pack_id"];
      $pconsumer = new DTProviderConsumer("gear","pack.php",$dt_token);
      $pack=$pconsumer->request("by_id",array("id"=>$pack_id));
      $out .= $mView->renderEach( function($obj){
        global $pack;
      	$moduleView = new ModuleView();
      	return $moduleView->renderModalOverview($obj,$pack);
      });
    } else {
      $out = '<div class="grid">';
      $out .= $mView->renderEach( function($obj){
      	$moduleView = new ModuleView();
      	return $moduleView->renderOverview($obj);
      });
      $out .= '</div>';
    }
    $response->obj = $out;
    $response->respond($params);
		return $response;
	} else {
  	$response = $consumer->requestAndRespond($params);
	}