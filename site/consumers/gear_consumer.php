<?php
  
	require_once dirname(__FILE__)."/../gear.inc.php";
	$params = $_REQUEST;
	$consumer = new DTProviderConsumer("gear","gear.php");
	$dt_token = $params['tok'];
	if($params["act"]=="addgear"){ //we have some post-processing to do
    $response = $consumer->requestAndRespond($params);
	}	else if($params["act"]=="search"){ //we have some post-processing to do
  	$response = new DTResponse($consumer->request($params["act"],$params));
    $gView = new GearListView();
    $gView->setItems($response->obj['items']);

    if(isset($params["pack_id"])) {//request for pack's gear
      $pack_id = $params["pack_id"];
      $consumer = new DTProviderConsumer("gear","pack.php",$dt_token);
      $pack=$consumer->request("by_id",array("id"=>$pack_id));
      $out = $gView->renderModalView($pack);
    } else if(isset($params["module_id"])) {//request for module's gear
      $module_id = $params["module_id"];
      $consumer = new DTProviderConsumer("gear","module.php",$dt_token);
      $module=$consumer->request("by_id",array("id"=>$module_id));
      $out = $gView->renderModalView($module);
    } else if(isset($params['ismodal'])) {
      $out = $gView->renderModalView();
    } else {
      $out = $gView->renderEditView();
    }
    $response->obj = $out;
    $response->respond($params);
		return $response;
	} else {
  	$response = $consumer->requestAndRespond($params);
	}