<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	$params = $_REQUEST;
	$consumer = new DTProviderConsumer("gear","pack.php");
	//$response = $consumer->requestAndRespond($params);
  if($params["act"]=="search"){ //we have some post-processing to do
  	$response = new DTResponse($consumer->request($params["act"],$params));
    $pView = new VCList(new DTParams());
    $pView->setItems($response->obj['items']);
    $out = '<div class="grid">';
    $out .= $pView->renderEach( function($obj){
    	$packView = new PackView();
    	return $packView->renderOverview($obj);
    });
    $out .= '</div>';
    $response->obj = $out;
    $response->respond($params);
		return $response;
	} else {
  	$response = $consumer->requestAndRespond($params);
	}