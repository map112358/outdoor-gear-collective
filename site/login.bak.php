<?php
session_start();
require('gear.inc.php');
require('functions.php');

//track whether an error occured
$error = 0;

if($_SERVER["HTTPS"] != "on") {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

if(isset($_SESSION['username'])) {
  header("Location: index.php");
  exit();
} else if (isset($params['username']) && isset($params['password'])) {
  try {
    $user = new Users($db->filter(array("username"=>strtolower($params['username']))));
    if(crypt($params['password'],$user['password']) == $user['password']) {
      $_SESSION['username'] = strtolower($params['username']);
      header("Location: index.php");
    } else {
      $error=true;
    }
  } catch(Exception $e){ DTLog::debug("Failed to get User (%s):\n%s",$e->getMessage(),$params->allParams()); $error=true;}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="css/gear.css">
  <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>  
 <div class="container" style="margin-top:40px">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong> Sign in to continue</strong>
					</div>
					<div class="panel-body">
						<form role="form" action="login.php" method="POST">
							<fieldset>
  							<div class="form-group">
									<div class="input-group text-danger">
  									<?php
										if ($error) {echo"Incorrect username or password";}
										?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-user"></i>
										</span> 
										<input class="form-control" placeholder="Username" name="username" type="text" autofocus>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-lock"></i>
										</span>
										<input class="form-control" placeholder="Password" name="password" type="password" value="">
									</div>
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-lg btn-default btn-block" value="Sign in">
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer ">
						Don't have an account? <a href="register.php" onClick="">Sign Up Now</a>
					</div>
                </div>
			</div>
		</div>
	</div>
<?php
include_once("postpage.php")
?>
