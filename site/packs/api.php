<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepageAPI);
	
	
  $page = $params->intParam('page');
  $callback = $params->stringParam('callback');
  $pConsumer = new DTProviderConsumer("gear","pack.php",$dt_token);
  $pView = new VCList($params);
  $pView->requestItems($pConsumer);
  $out = $pView->renderEach( function($obj) use ($page){
  	$packView = new PackView();
  	return $packView->renderOverview($obj,$page);
  });
  
  
  
  $f = array("fmt"=>"DTR","err"=>0,"obj"=>$out);
  echo $callback ."(". json_encode($f) .")";
