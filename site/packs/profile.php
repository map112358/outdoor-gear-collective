<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	$pack_id=$params->intParam("id");
	$consumer = new DTProviderConsumer("gear","pack.php",$dt_token);
	$o=$consumer->request("by_id",array("id"=>$pack_id));
	$view = new PackView();
	$is_owner = $user['id'] == $o['user']['id'];
?>
<div class="container main-container">
  <?php
    if($is_owner)
      echo $view->renderProfile($o,true);
    else
      echo $view->renderProfile($o);
  ?>
</div>
<script>
  // Each time the user scrolls in modal

  var gwin = $(".gear-pack-modal");
  var gpage = 2;
	gwin.scroll(function() {
		if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
      console.log("get gear modal data");
    	populateGearPackModal('<?=DTSettingsConfig::baseURL("gear/api.php")?>','<?=$dt_token?>',gpage,<?=$params['id']?>,'#modal-gear-pack-table tbody')
      gpage += 1;
    }
  });  
  
  var mwin = $(".module-pack-modal");
  var mpage = 2;
	mwin.scroll(function() {
		if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
      console.log("get module modal data");
    	populateModulePackModal('<?=DTSettingsConfig::baseURL("modules/api.php")?>','<?=$dt_token?>',mpage,<?=$params['id']?>,'#modal-module-pack','<?=$params["like"]?>')
      mpage += 1;
    }
  });
  
  $(document).ready(function() {
    resizeTextAreaEvent();
    fresh();
  });
</script>
<?php
require($postpage);