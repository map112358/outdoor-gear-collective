<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	$id=$params->intParam("id");
	$consumer = new DTSecureProviderConsumer("gear","pack.php",$dt_token);
	$obj = $consumer->request("by_id",array("id"=>$id));
	
?>

<h1>Edit Gear</h1>

<form class='form' role="form" action='javascript:dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/pack_consumer.php")?>","form":"form"})'>
	<input type="hidden" name="act" value="update" />
	<input type="hidden" name="tok" value="<?=$dt_token?>" />
	<?=$view->renderView($obj)?>
	<input type="submit" class='btn btn-primary pull-right' value="Save" />
</form>


<?php
	require($postpage);