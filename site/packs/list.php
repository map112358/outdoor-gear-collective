<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	$pConsumer = new DTProviderConsumer("gear","pack.php",$dt_token);
  $pView = new VCList($params);
  $pView->requestItems($pConsumer);

?>

<div class="container-fluid main-container">
  <div class="row">
    <div class="col-xs-12">
      <h3>Packs</h3>
      <div class="container-fluid">
        <div class="form-group">
        	<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchModels('<?=DTSettingsConfig::baseURL("consumers/pack_consumer.php")?>', '<?=$dt_token?>', $(this).val().toLowerCase(), '.pack-grid')">
        </div>
      </div>
      <div class="pack-grid">
        <div class="grid">
          <?=$pView->renderEach( function($obj){
          	$packView = new PackView();
          	return $packView->renderOverview($obj);
          });?>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var page = 2;
  $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
      console.log("get pack data");
    	populatePage('<?=DTSettingsConfig::baseURL("packs/api.php")?>','<?=$dt_token?>',page,'.grid',$('#search').val().toLowerCase());
      page += 1;
    }
  });
  
  $(document).ready(function() {
    resizeTextAreaEvent();
    fresh();
  });
</script>

<?php
	require($postpage);