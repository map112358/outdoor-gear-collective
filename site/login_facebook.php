<?php
require_once dirname(__FILE__)."/site.inc.php";
$session = DTSession::sharedSession();
$dt_token = DTAPI::consumerTokenForAPI("gear");

//make sure we have a request token to authorize
$oauth_token = htmlentities($_REQUEST["oauth_token"]);
if(empty($oauth_token)){ // we need to have this first...
	header("Location: index.php");
	exit();
}

$tok=DTAPI::consumerTokenForAPI("facebook");
$consumer = new FacebookConsumer("facebook","",$tok);
$profile = $consumer->request("me");

if(isset($profile,$profile["id"])){
	setcookie("login_method","facebook",time()+60*60*24*3000);
	$consumer = new DTProviderConsumer("gear","authentication.php",$dt_token);
	$consumer->request("authorize_fb",array("fb_acc"=>$session["facebook_oauth_access_token"],"req_tok"=>$oauth_token,"fbid"=>$profile["id"]));
}
// we should be redirected to the DT verifier at this point
?>

<script>
history.back();
</script>