<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepageAPI);
	
	
  $page = $params->intParam('page');
  $callback = $params->stringParam('callback');
	$consumer = new DTProviderConsumer("gear","gear.php",$dt_token);
  $gView = new GearListView($params);
  $gView->requestItems($consumer);
  
  $pack_id = $params->intParam("pack_id");
  $module_id = $params->intParam("module_id");
  if($pack_id != 0) {//request for pack's gear
    $consumer = new DTProviderConsumer("gear","pack.php",$dt_token);
    $pack=$consumer->request("by_id",array("id"=>$pack_id));
    $out = $gView->renderModalRowView($pack);
    $f = array("fmt"=>"DTR","err"=>0,"obj"=>$out);
    echo $callback ."(". json_encode($f) .")";
  } else if($module_id != 0) {//request for module's gear
    $consumer = new DTProviderConsumer("gear","module.php",$dt_token);
    $module=$consumer->request("by_id",array("id"=>$module_id));
    $out = $gView->renderModalRowView($module);
    $f = array("fmt"=>"DTR","err"=>0,"obj"=>$out);
    echo $callback ."(". json_encode($f) .")";
  } else {//request for gear
    $consumer = new DTProviderConsumer("gear","module.php",$dt_token);
    $module=$consumer->request("list");
    $out = $gView->renderPage();
    $f = array("fmt"=>"DTR","err"=>0,"obj"=>$out);
    echo $callback ."(". json_encode($f) .")";
  }