<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	$consumer = new DTProviderConsumer("gear","gear.php",$dt_token);
  $gView = new GearListView($params);
  $gView->requestItems($consumer);

?>

<div class="container-fluid main-container">
  <div class="row">
    <div class="col-xs-12">
      <h3>Gear</h3>
      <div class="form-group">
      	<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchModels('<?=DTSettingsConfig::baseURL("consumers/gear_consumer.php")?>', '<?=$dt_token?>', $(this).val().toLowerCase(), '.gear-grid')">
      </div>
      <div class="gear-grid">
        <?=$gView->renderView();?>
      </div>
    </div>
  </div>
</div>
<script>
	// Each time the user scrolls in modal
  var page = 2;
  $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
    	populatePage('<?=DTSettingsConfig::baseURL("gear/api.php")?>','<?=$dt_token?>',page,'#gear-table tbody',$('#search').val().toLowerCase())
      page += 1;
    }
  });
</script>
<?php
	require($postpage);