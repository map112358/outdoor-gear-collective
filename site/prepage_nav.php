
<?php ?>
<nav class="navbar navbar-default navbar-static-top nav-padding no-margin-bottom">
      
      <div class="container-fluid">
        <a class="navbar-brand no-padding-top" href="<?=DTSettingsConfig::baseURL("home.php")?>">
          <img class="nav-logo" src="<?=DTSettingsConfig::baseURL("img/gearCrossing_logo.png")?>">
        </a>

        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?=DTSettingsConfig::baseURL("packs/list.php")?>">Packs</a></li>
            <li><a href="<?=DTSettingsConfig::baseURL("modules/list.php")?>">Modules</a></li>
            <li><a href="<?=DTSettingsConfig::baseURL("gear/list.php")?>">Gear</a></li>
            <li><a href="<?=DTSettingsConfig::baseURL("users/list.php")?>">Hikers</a></li>
        <!--    <li><a href="<?=DTSettingsConfig::baseURL("stats.php")?>">Stats</a></li> 
            <li><a href="<?=DTSettingsConfig::baseURL("store.php")?>">Store</a></li> -->
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li>
              <img class="img-circle navbar-avatar pointer" onclick='window.location = "<?=DTSettingsConfig::baseURL("users/profile.php?id=".$user['id'])?>"' alt="" src="<?=DTSettingsConfig::baseURL("uploads/".$user['image']['filename'])?>" onerror="if (this.src != '<?=DTSettingsConfig::baseURL("img/default_user.png")?>') this.src = '<?=DTSettingsConfig::baseURL("img/default_user.png")?>';">
            </li>
            <li class="dropdown">
              <a href="<?=DTSettingsConfig::baseURL("#")?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class='pull-right glyphicon glyphicon-cog navbar-cog'></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?=DTSettingsConfig::baseURL("index.php?action=logout")?>">Logout</a></li>
                <li><a href="<?=DTSettingsConfig::baseURL("#")?>">Settings</a></li>
                <li><a href="<?=DTSettingsConfig::baseURL("#")?>">Report Issues</a></li>
              </ul>
            </li>
            <li>
              <!-- <div class="navbar-search">
                <input type="text">
              </div> -->
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
