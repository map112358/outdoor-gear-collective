<?php
	require_once dirname(__FILE__)."/gear.inc.php";
	require($prepageMain);
	
	$session = DTSession::sharedSession();
	
	//disable OAuth for GoDaddy host
	if(isset($_REQUEST["oauth_token"]) && $_REQUEST["oauth_token"] != "")
		$oauth_token = htmlentities($_REQUEST["oauth_token"]);
	else //never come here directly without requesting a token first
		echo "<script>window.location.replace('users/edit.php');</script>";
		
	//disable SSO login for GoDaddy host
	/*if(isset($_COOKIE["login_method"])){
		switch($_COOKIE["login_method"]){
			case "facebook":
				echo "<script>window.location.replace('".DTSettingsConfig::baseURL("login_facebook.php")."')</script>";
				exit();
			case "google":
				echo "<script>window.location.replace('".DTSettingsConfig::baseURL("login_google.php")."')</script>";
				exit();
			case "twitter":
				echo "<script>window.location.replace('".DTSettingsConfig::baseURL("login_twitter.php")."')</script>";
				exit();
		}
	}*/
?>
<div class="container main-container">
  <div class='col-lg-4 col-md-3 col-xs-0'></div>
  <div class='row white-box col-lg-4 col-md-6 col-xs-12'>
  	<img class="index-logo" src="<?=DTSettingsConfig::baseURL("img/gearCrossing_logo.png")?>">
  	
    <div class='row'>
      <div class='col-xs-12'>
        <a class="tour-btn btn btn-green col-xs-12" href="<?=DTSettingsConfig::baseURL("home.php")?>">Take a tour</a>
      </div>
    </div>
  	
  	<form class='form formRegister row' role="form" action='javascript:dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/users_consumer.php")?>","form":".formRegister"})'>
  		<input type="hidden" name="id" value="<?=$user["id"]?>" />
  		<input type="hidden" name="act" value="register" />
  		<input type="hidden" name="tok" value="<?=$dt_token?>" />
  		<div class="col-sm-12">
  			<span class='index-label'>Sign Up</span>
  			<div class="row">
  				<div class="padding-right-sm col-sm-6 form-group">
  					<input type="text" placeholder="First Name" class="form-control " name="name_first" required>
  				</div>
  				<div class="padding-left-sm col-sm-6 form-group">
  					<input type="text" placeholder="Last Name" class="form-control" name="name_last" required>
  				</div>
  			</div>					
  			<div class="form-group">
  				<input type="email" placeholder="Email Address" class="form-control" name="alias" required  
  				  data-fv-remote=true
						data-fv-remote-data='{"act":"valid_user","tok":"<?=$dt_token?>","fmt":"json"}'
						data-fv-remote-delay=1000
						data-fv-remote-message="This email address is already registered."
						data-fv-remote-url="<?=DTSettingsConfig::baseURL("consumers/users_consumer.php");?>"
						data-fv-regexp="true"
		                data-fv-regexp-regexp="^[@a-z0-9_\-\.]+$"
		                data-fv-regexp-message="Email cannot contain uppercase, spaces, or special characters.">
  			</div>
  			<div class="row">
  				<div class="padding-right-sm col-sm-6 form-group">
  					<input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
  				</div>
  				<div class="padding-left-sm col-sm-6 form-group">
  					<input type="password" placeholder="Confirm Password" class="form-control" name="verify" id="verify" required data-fv-identical="true" data-fv-identical-field="password" data-fv-identical-message="must match password">
  				</div>
  			</div>
  			<input type="submit" value="Register" class='btn btn-green pull-right' />
  		</div>
  	</form>
    <form class='form formLogin row' role="form" action='javascript: dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/authentication_consumer.php")?>","form":".formLogin"});'>
      <div class="col-sm-12">
  			<span>Sign In</span>
  			<input type="hidden" name="act" value="authenticate" />
  			<input type="hidden" name="tok" value="<?=$dt_token?>" />
  			<input type="hidden" name="oauth_token" value="<?=$oauth_token?>" />		
  			<div class='form-group'>
  				<input type="email" class='form-control' name="alias" id="alias" placeholder="Email Address"/>
  			</div>
  			<div class='form-group'>
  				<input type="password" class='form-control' name="password" id="password" placeholder="Password" />
  			</div>
  			<a href="recovery.php">Forgot your password?</a>
  			<input type="submit" value="Login" class='btn btn-green pull-right' />
     <!--
  			<span>Social Media Account</span>
  			<p><a href="<?=DTSettingsConfig::baseURL("login_facebook.php?oauth_token={$oauth_token}")?>" class='btn btn-default' style='width: 100%'><img class='pull-left' src="<?=DTSettingsConfig::baseURL("img/facebook.png")?>" /></span> Login with Facebook</a></p>
  			<p><a href="<?=DTSettingsConfig::baseURL("login_twitter.php?oauth_token={$oauth_token}")?>" class='btn btn-info' style='width: 100%'><img class='pull-left' src="<?=DTSettingsConfig::baseURL("img/twitter.png")?>" /></span> Login with Twitter</a></p>
  			<p><a href="<?=DTSettingsConfig::baseURL("login_google.php?oauth_token={$oauth_token}")?>" class='btn btn-danger' style='width: 100%'><img class='pull-left' src="<?=DTSettingsConfig::baseURL("img/google_plus.png")?>" /></span> Login with Google+</a></p>
  			-->
  		</div>
  	</form>
  </div>
  <div class='col-lg-4 col-md-3 col-xs-0'></div>
</div>
<?php
	include $postpageMain;