<?php
  if($is_user_profile) {
?>
<form>
  <div class="form-group has-feedback">
    <textarea placeholder="Tell us something about yourself" class="form-control max-width" name="about" id="about" required><?=$user['about']?></textarea>
  </div>
  <input type="submit" class="btn btn-success"/>
</form>
<?php
  } else {
?>
  <p><?=$user['about']?></p>
<?php
  }
?>