<?php
  $mView = new VCList($params);
  $mView->setItems($o['following']);
?>

<div class="grid">
<?=$mView->renderEach( function($obj){
	$profileView = new UserProfileView(new DTParams());
  return $profileView->renderUserCard($obj);
});?>
</div>