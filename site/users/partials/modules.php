<?php
  $mView = new VCList($params);
  $mView->setItems($o['modules']);
?>
<div class="grid">
<?php
  if($is_user_profile) {
    $pv = new ModuleView();
    echo $pv->renderAddModule($user['id'],$dt_token);
    echo $mView->renderEach( function($obj) {
      global $user;
    	$moduleView = new ModuleView();
    	return $moduleView->renderAdminOverview($obj,$user);
    });
  } else {
    echo $mView->renderEach( function($obj){
    	$moduleView = new ModuleView();
    	return $moduleView->renderOverview($obj);
    });
  }
?>
</div>