<?php
  $pView = new VCList($params);
  $pView->setItems($o['packs']);
?>
<div class="grid">
<?php
  if($is_user_profile) {
    $pv = new PackView();
    echo $pv->renderAddPack($user['id'],$dt_token);
    echo $pView->renderEach( function($obj){
      global $user;
    	$packView = new PackView();
    	return $packView->renderAdminOverview($obj,$user);
	  });
  } else {
    echo $pView->renderEach( function($obj){
    	$packView = new PackView();
    	return $packView->renderOverview($obj);
	  });
  }
?>
</div>