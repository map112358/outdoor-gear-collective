<?php 
  if($is_user_profile){
?>
<div id="banner-pic-button" class="container">
  <button class="btn btn-green" onclick="$('#banner-picture-upload').focus().trigger('click')">Change Banner</button>
</div>
<?php } ?>
<div class="profile-banner" style="background-image:url(<?=DTSettingsConfig::baseURL("uploads/".$o['image_header']['filename'])?>),url(<?=DTSettingsConfig::baseURL("img/banner-test.jpg")?>)">
</div>

<div class="profile-hiker-name">
  <h4><?=$o['name_first']?> <?=$o['name_last']?></h4>
</div>
<div class="profile-banner-bar ">
  <div class="container-fluid">
    
    <?php 
      if($is_user_profile){
    ?>
        <form id='profile-pic-form' role="form" action='javascript:dt.upload({"url":"<?=DTSettingsConfig::baseURL("consumers/secure/users_consumer.php")?>","success":function(){window.location.reload()},"error":function(){window.location.reload()},"form":"#profile-pic-form"})'>
          <input type="hidden" name="user_id" value="<?=$user_id?>">
          <input type="hidden" name="pic_type" value="profile">
          <input type="hidden" name="act" value="update" />
  			  <input type="hidden" name="tok" value="<?=$dt_token?>" />
          <input type='file' name='profile-image' id='profile-picture-upload' onchange="javascript:this.form.submit();"/>
        </form>
        <form id='banner-pic-form' role="form" action='javascript:dt.upload({"url":"<?=DTSettingsConfig::baseURL("consumers/secure/users_consumer.php")?>","success":function(){window.location.reload()},"error":function(){window.location.reload()},"form":"#banner-pic-form"})'>
          <input type="hidden" name="user_id" value="<?=$user_id?>">
          <input type="hidden" name="pic_type" value="banner">
          <input type="hidden" name="act" value="update" />
  			  <input type="hidden" name="tok" value="<?=$dt_token?>" />
          <input type='file' name='banner-image' id='banner-picture-upload' onchange="javascript:this.form.submit();"/>
        </form>
    <?php
      } else if($is_logged_in) { 
        if(!$is_friend) {
    ?> 
    <div class="pull-right">
      <form class='form follow-form' id="follow-form" role="form" action='javascript:dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/secure/users_consumer.php")?>","success":function(){window.location.reload()},"form":"#follow-form"})'>
        <input type="hidden" name="user_id" value="<?=$user_id?>">
        <input type="hidden" name="act" value="follow" />
			  <input type="hidden" name="tok" value="<?=$dt_token?>" />
      </form>
      <a class="btn btn-outline" href="javascript:document.getElementById('follow-form').submit();" onclick="$('.follow-form').submit()">Follow</a>
    </div>
    <?php
      } else {?>
      <div class="pull-right">
        <form class='form follow-form' id="follow-form" role="form" action='javascript:dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/secure/users_consumer.php")?>","success":function(){window.location.reload()},"form":"#follow-form"})'>
          <input type="hidden" name="user_id" value="<?=$user_id?>">
          <input type="hidden" name="act" value="unfollow" />
  			  <input type="hidden" name="tok" value="<?=$dt_token?>" />
        </form>
        <a class="btn btn-outline" href="javascript:document.getElementById('follow-form').submit();" onclick="$('.follow-form').submit()">Following</a>
      </div>
     <?php
      }
    }
    ?>
    <div class="">
      <!-- <h3 class="col-xs-offset-3 col-sm-offset-2 col-xs-8 col-sm-9"><?=$o['name_first']." ".$o['name_last'] ?></h3> -->
    </div>
    <div class="navbar-header">
      
      <button type="button" class="btn-outline navbar-toggle collapsed" data-toggle="collapse" data-target="#profile-navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand no-padding avatar avatar-profile">
        <?php if($is_user_profile){ ?>
        <img class="pointer" onclick="$('#profile-picture-upload').focus().trigger('click')" alt="" src="<?=DTSettingsConfig::baseURL("uploads/".$o['image']['filename'])?>" onerror="if (this.src != '<?=DTSettingsConfig::baseURL("img/default_user.png")?>') this.src = '<?=DTSettingsConfig::baseURL("img/default_user.png")?>';">
        <?php } else { ?>
        <img alt="" src="<?=DTSettingsConfig::baseURL("uploads/".$o['image']['filename'])?>" onerror="if (this.src != '<?=DTSettingsConfig::baseURL("img/default_user.png")?>') this.src = '<?=DTSettingsConfig::baseURL("img/default_user.png")?>';">
        <?php } ?>
      </div>
    </div>
    <div id="profile-navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a class="pointer profile-nav" onclick="display('#profile-hikers')">Following</a></li>
        <li><a class="pointer profile-nav" onclick="display('#profile-packs')">Packs</a></li>
        <li><a class="pointer profile-nav" onclick="display('#profile-modules')">Modules</a></li>
        <li><a class="pointer profile-nav" onclick="display('#profile-gear')">Gear</a></li>
      </ul>
    </div>
  </div>
</div>
<br>
<br>