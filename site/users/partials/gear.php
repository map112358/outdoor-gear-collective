<?php
  $gView = new GearListView($params);
  $gView->setItems($o['gear']);
?>

<?=$gView->renderView($o['gear']);?>
<div id='createGearModal' class='modal fade'>
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
        <h4 class="modal-title">Create Gear</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
      		<h3>Missing your favorite gear?</h3>
          <form action='' method="POST">
            <div class="form-group">
          		<input type="text" placeholder="Name" class="form-control" name="name" required>
          	</div>
          	<div class="form-group">
          		<input type="number" placeholder="weight (oz)" class="form-control" name="weight" required>
          	</div>
            <div class="form-group">
            	<select class="form-control" name="brand_id" required>
              	<option value="" disabled selected>Brand</option>
            		<?php
              		//
              		// get the brands from provider
              		//
                  //$brand = Brand::select($db->filter()->orderby("name ASC"));
                  $brand = array();
                  foreach($brand AS $b) {
                    echo "<option value='{$b['id']}'>".$b['name']."</option>";
                  }
                ?>
            	</select>
            </div>
            <div class="form-group">
            	<select class="form-control" name="category_id" required>
              	  <option value="" disabled selected>Category</option>
            		<?php
              		//
              		// get the categories from provider
              		//
                  //$category = ModuleCategory::select($db->filter()->orderby("name ASC"));
                  $category = array();
                  foreach($category AS $c) {
                    echo "<option value='{$c['id']}'>".$c['name']."</option>";
                  }
                ?>
            	</select>
            </div>
        
          	<input type='hidden' name='type' value='create_gear'>
            <input class="btn btn-default" type="submit" name="submit" value="Submit">
          </form>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
      </div>
    </div>
  </div>
</div>