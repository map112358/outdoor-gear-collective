<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	$user_id=$params->intParam("id");
  if($user_id == 0) {
    $consumer = new DTSecureProviderConsumer("gear","secure/users.php",$dt_token);
    $obj = $consumer->request("by_id",array("id"=>$user['id']));
  }
	$consumer = new DTProviderConsumer("gear","users.php",$dt_token);
	$o=$consumer->request("by_id",array("id"=>$user_id));
	$is_user_profile = $user_id == $user['id'];
	$is_logged_in = isset($user['id']);
	
	function isFriend($id, $array) {
    foreach ($array as $key => $val) {
      if ($val['id'] === $id) {
        return true;
      }
    }
    return false;
  }

	$is_friend = isFriend($user_id,$user['following']);
?>

<?php
 require("partials/user_banner.php");  
?>

<div class='container-fluid main-container'>
  
  <div id="profile-packs" class="profile-body profile-packs">
    <?php include("partials/packs.php"); ?>
  </div>
  
  <div id="profile-modules" class="profile-body profile-modules">
    <?php include("partials/modules.php"); ?>
  </div>
  
  <div id="profile-hikers" class="profile-body profile-hikers">
    <?php include("partials/friends.php"); ?>
  </div>
  
  <div id="profile-gear" class="col-xs-12 profile-body profile-gear">
    <?php include("partials/gear.php"); ?>
  </div>
  
  <div id="profile-about" class="col-xs-12 profile-body profile-about">
    <?php //include("partials/about.php"); ?>
  </div>  
  <div id="profile-hikes" class="col-xs-12 profile-body profile-hikes">
    <?php //include("partials/hikes.php"); ?>
  </div>  
</div>
  
<script>
  function displayClassName(d) {
    $(".profile-body").hide().removeClass("profile-nav-selected");
    $(d).show().addClass("profile-nav-selected");
  }
  function display(d) {
    displayClassName(d)
    fresh();
    pageXOff = window.pageXOffset > 0 ? window.pageXOffset : 0;
    pageYOff = window.pageYOffset > 0 ? window.pageYOffset : 0;
    window.location.hash = d;      
    window.scrollTo(pageXOff, pageYOff);
  }
  $(document).ready(function() {
    resizeTextAreaEvent();
    if(window.location.hash)
      display(window.location.hash);  
    else
      displayClassName("#profile-packs");
  });
</script>

<?php
require($postpage);