<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepageMain);
	
	$id=$params->intParam("id");
	$consumer = new DTSecureProviderConsumer("gear","secure/users.php",$dt_token);
	$obj = $consumer->request("by_id",array("id"=>$user['id']));
	
	$view = new UserProfileView();
	header("Location: ".DTSettingsConfig::baseURL("users/profile.php?id=".$user['id']));
?>

<?php
	require($postpage);