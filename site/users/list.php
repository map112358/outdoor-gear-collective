<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	$consumer = new DTProviderConsumer("gear","users.php",$dt_token);
	$view = new VCList($params);
	$view->requestItems($consumer);
?>
<div class="container-fluid main-container">
  <div class='row'>
  	<div class='col-md-12'>
  		<h3>Users</h3>
  	</div>
	  <div class="container-fluid">
  	  <div class="col-xs-12">
        <div class="form-group">
        	<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchModels('<?=DTSettingsConfig::baseURL("consumers/users_consumer.php")?>', '<?=$dt_token?>', $(this).val().toLowerCase(), '.user-grid')">
        </div>
  	  </div>
    </div>
  </div>
  <div class='user-grid'>
    <?=$view->renderEach( function($obj){
      $profileView = new UserProfileView(new DTParams());
      return $profileView->renderUserCard($obj);
    }); ?>
  </div>
</div>

<?php
	require($postpage);
	
	