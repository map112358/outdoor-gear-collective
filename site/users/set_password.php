<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	/* This page requires a user alias and valid appkey, and will set the password appropriately */
	$message = $params->stringParam("message");
	$alias = urldecode($params->stringParam("alias"));
	$rst = $params->stringParam("rst");
	
	
?>

<div class='container' style='padding-top: 100px'>
	<div class='row'>
		<div class='col-md-offset-3 col-md-6 black-box'>
			<h1>Set Password</h1>
			
			<?php if($message=="verified"){ ?>
				<h2>Email Address Verified</h2>
				<p>Your email address has been verified. You may now choose your password.</p>
			<?php } ?>
			
			<p>Please choose a new password for your account.</p>
			<form id="password_reset_form" action="javascript:dt.post({form:'#password_reset_form',url:'<?=DTSettingsConfig::baseURL("consumers/authentication_consumer.php")?>',success:function(response){window.location.href='/index.php';}})">
				<input type="hidden" name="rst" value="<?=$rst?>" />
				<input type="hidden" name="alias" value="<?=$alias?>" />
				<input type="hidden" name="tok" value="<?=$dt_token?>" />
				<input type="hidden" name="act" value="reset_password" />
				<fieldset>
					<legend>New Password</legend>
					<div class='row'>
						<div class='col-sm-12'>
							<div class='form-group'>
								<label for="password">Password</label>
								<input type="password" name="password" class='form-control' id='password' placeholder="Password" required minlength=6 />
							</div>
						</div>
					</div>
					<div class='row'>
						<div class='col-sm-12'>
							<div class='form-group'>
								<label for="verify">Verify</label>
								<input type="password" name="verify" class='form-control' id='verify' placeholder="Verify Password" data-fv-identical="true" data-fv-identical-field="password" data-fv-identical-message="must match password" />
							</div>
						</div>
					</div>
					<input class='btn btn-primary pull-right' type="submit" value="Set Password" id="submit_button" />
				</fieldset>
			</form>
		</div>
	</div>
</div>

<script>
	//remove the sidebar margin on this page
	$(".wrapper").css("margin","0px");
</script>

<?php
	require($postpage);