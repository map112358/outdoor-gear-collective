<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	$view = new UserProfileView();
?>

<div class='container'>
	<div class='row'>
		<div class='col-md-12'>

			<h1>Register a New User</h1>
			
			<form class='form' role="form" action='javascript:dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/users_consumer.php")?>","form":"form"})'>
				<input type="hidden" name="id" value="<?=$user["id"]?>" />
				<input type="hidden" name="act" value="register" />
				<input type="hidden" name="tok" value="<?=$dt_token?>" />
				<?=$view->renderView($user)?>
				<input type="submit" class='btn btn-primary pull-right' value="Save" />
			</form>
		</div>
	</div>
</div>

<?php
	require($postpage);