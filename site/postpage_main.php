<?php
  ?>
<script>
$(document).ready(function() { 
  $(".table-sort").each(function(){
    $(this).tablesorter();
  });
  fresh();
}); 
</script>

<script>
		$(document).ready(function(){
		//$("select").select2({allowClear: true});
		//$(".date").datepicker({changeYear: true, yearRange:"-100:+0"});
		$('form').formValidation({ framework: 'bootstrap',icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
            excluded: [".exclude-validator",':disabled', ':hidden', ':not(:visible)']
		}}).on('success.field.fv', function(e,data) {
			data.fv.disableSubmitButtons(false);
		}).on('err.field.fv', function(e,data) {
			data.fv.disableSubmitButtons(true);
		});
	});  
</script>