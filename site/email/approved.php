<?php require_once dirname(__FILE__)."/premail.php"; ?>
		<p align=right><?=$date?></p>
		<h3>Account Approved</h3>
		<p>Dear Hiker,</p>
		
		<p>Congratulations! Your account has been approved for the Gear Crossing!</p>
		
		<p>If you did not request to register with Gear Crossing, please disregard this email. For questions or concerns, please email us at <a href="mailto:alfp@missouri.edu">gearcrossing@gmail.com</a>.</p>
<?php require_once dirname(__FILE__)."/postmail.php"; ?>