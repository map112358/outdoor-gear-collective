<?php require_once dirname(__FILE__)."/premail.php"; ?>
	<h3>Gear Crossing Password Reset Verification</h3>
	
	<p>There has been a request to reset the password for your Gear Crossing account. If you did not request a password reset, please disregard this email. Otherwise, you may verify this request and reset your password by clicking the link below:</p>
	
	<p><a href="<?=$email_reset_url?>"><?=$email_reset_url?></a></p>
	
	<p>If you have any questions or concerns, please contact us at gearcrossing@gmail.com. Gear Crossing and its affiliates will never ask you for your account password.</p>
<?php require_once dirname(__FILE__)."/postmail.php"; ?>