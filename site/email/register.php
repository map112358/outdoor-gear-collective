<?php require_once dirname(__FILE__)."/premail.php"; ?>
	<p align=right><?=$date?></p>
	<h3>Gear Crossing Registration Received</h3>
	<p>Dear <?=$email_therapist_name?>,</p>
	
	<p>Thank you for registering with Gear Crossing!</p>
	
	<div class='highlight_box'>
	<h2>Next Steps:</h2>
	
	<p><span style='font-size:1.5em'>1.</span> Please <a href="<?=$email_reset_url?>">create a password for your account</a>.</p>
	
<?php require_once dirname(__FILE__)."/postmail.php"; ?>