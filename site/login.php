<?php
	require_once dirname(__FILE__)."/gear.inc.php";
	include $prepage;
	
	$session = DTSession::sharedSession();
	
	//disable OAuth for GoDaddy host
	if(isset($_REQUEST["oauth_token"]) && $_REQUEST["oauth_token"] != "")
		$oauth_token = htmlentities($_REQUEST["oauth_token"]);
	else //never come here directly without requesting a token first
		echo "<script>window.location.replace('users/edit.php');</script>";
		
	//disable SSO login for GoDaddy host
	/*if(isset($_COOKIE["login_method"])){
		switch($_COOKIE["login_method"]){
			case "facebook":
				echo "<script>window.location.replace('".DTSettingsConfig::baseURL("login_facebook.php")."')</script>";
				exit();
			case "google":
				echo "<script>window.location.replace('".DTSettingsConfig::baseURL("login_google.php")."')</script>";
				exit();
			case "twitter":
				echo "<script>window.location.replace('".DTSettingsConfig::baseURL("login_twitter.php")."')</script>";
				exit();
		}
	}*/
?>

<div class='col-lg-4 col-md-3 col-xs-0'></div>
<div class='row white-box col-lg-4 col-md-6 col-xs-12'>
	<h3 class='section-header'>Gear Crossing Login<span class='pull-right glyphicon glyphicon-lock'></span></h3>
	<div class='col-md-12'>
		<h4>Secure Access</h4>
		<p>This section of the website requires a user account. If you have been given an account, please log in using one of the methods below.</p>
		<form role="form" action='javascript: dt.post({"url":"<?=DTSettingsConfig::baseURL("consumers/authentication_consumer.php")?>","form":"form"});'>
			<fieldset>
				<legend>User Login</legend>
				<input type="hidden" name="act" value="authenticate" />
				<input type="hidden" name="tok" value="<?=$dt_token?>" />
				<input type="hidden" name="oauth_token" value="<?=$oauth_token?>" />
				<div class='form-group'>
					<label for="alias">User Account</label>
					<input type="text" class='form-control' name="alias" id="alias" placeholder="Username" />
				</div>
				<div class='form-group'>
					<label for="password">Password</label>
					<input type="password" class='form-control' name="password" id="password" placeholder="Password" />
				</div>
				<a href="recovery.php">Forgot your password?</a>
				<input type="submit" value="Login" class='btn btn-primary pull-right' />
			</fieldset>
			<fieldset>
				<legend>Social Media Account</legend>
				<p><a href="<?=DTSettingsConfig::baseURL("login_facebook.php?oauth_token={$oauth_token}")?>" class='btn btn-default' style='width: 100%'><img class='pull-left' src="<?=DTSettingsConfig::baseURL("img/facebook.png")?>" /></span> Login with Facebook</a></p>
				<p><a href="<?=DTSettingsConfig::baseURL("login_twitter.php?oauth_token={$oauth_token}")?>" class='btn btn-info' style='width: 100%'><img class='pull-left' src="<?=DTSettingsConfig::baseURL("img/twitter.png")?>" /></span> Login with Twitter</a></p>
				<!--<p><a href="<?=DTSettingsConfig::baseURL("login_google.php?oauth_token={$oauth_token}")?>" class='btn btn-danger' style='width: 100%'><img class='pull-left' src="<?=DTSettingsConfig::baseURL("img/google_plus.png")?>" /></span> Login with Google+</a></p>-->
			</fieldset>
		</form>
	</div>
</div>
<div class='col-lg-4 col-md-3 col-xs-0'></div>

<?php
	include $postpage;