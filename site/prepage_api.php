<?php
// redirect to HTTPS as soon as possible, to avoid "header already sent" and unnecessary loading
// this will trip up invalid cert calls to provider... this should be done by the provider redirect
if($_SERVER["HTTPS"] != "on") {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

$params = new DTParams();
$session = DTSession::sharedSession(); // singleton session object (also calls session_start, if necessary)
$dt_token = DTAPI::consumerTokenForAPI("gear"); // token for accessing Deep Thought providers

$user = null;
$user_consumer = new DTSecureProviderConsumer("gear","secure/users.php",$dt_token);
if($user_consumer->accessToken()!="") // make sure we're already authenticated before we request user data
	$user = $user_consumer->request("current_user");
