<?php
require_once dirname(__FILE__)."/gear.inc.php";
require($prepage);

$pConsumer = new DTProviderConsumer("gear","packs.php",$dt_token);
$pView = new VCList($params);
$pView->requestItems($pConsumer);
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h3>Trending Packs</h3>
      <div class="grid">
  <?php
  ?>
  <?=$pView->renderEach( function($obj){
  	$packView = new PackView();
  	return $packView->renderOverview($obj);
  });?>
      </div>
    </div>
  </div>

<?php
/*  
  if (isset($params['id'])) {
    $pack = new Pack($db->filter(array("id"=>$params['id'])));
    if($pack['user_id']==$user['id']) {
      printPack($pack,array("pack"=>$pack, "carry"=>0, "owner"=>true, "moduleAdd"=>true));
    } else {
      printPack($pack);
    }
  } else if (isset($params['user_id'])) {
    $packs = Pack::select($db->filter(array("user_id"=>$params['user_id'])));//,"is_public"=>true)));
    if($params['user_id'] == $user['id']) {
      printPacks($packs,array("owner"=>true,"overview"=>true));
    } else {
      printPacks($packs,array("overview"=>true)); 
    }
  } else {
    echo "<h3>Community Packs</h3><h6><em>Trending Now</em></h6>";
    $packs = Pack::select($db->filter()->where("user_id <> ".$user['id'])->orderby("RANDOM()"));
    printPacks($packs,array("overview"=>true,"user_name"=>true));
  }*/
?>
</div>
<script>

$(document).ready(function() {
  resizeTextAreaEvent();
  fresh();
});
</script>


<?php
include_once("postpage.php")
?>