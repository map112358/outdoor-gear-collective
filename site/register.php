<?php
session_start();
require('../gear.inc.php');
require('functions.php');

if($_SERVER["HTTPS"] != "on") {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

if(isset($_SESSION['username'])) {
  header("Location: index.php");
  exit();
} else if (isset($params['username']) && isset($params['password']) && isset($params['namefirst']) && isset($params['namelast'])) {
  try {
    $user = new Users($db->filter(array("username"=>strtolower($params['username']))));
    //name taken
    echo "echo Username already taken";
  } catch(Exception $e) {
    $params1 = array("username"=>strtolower($params['username']), "password"=>crypt($params['password']), "name_first"=>$params['namefirst'], "name_last"=>$params['namelast']);
    $user = Users::upsert($db->filter(array("username"=>strtolower($params['username']))),$params1);
    DTLog::debug($params1);
    $db->commit();
    header("Location: index.php");
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="css/gear.css">
  <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>  
  <div class="container">
    <h1 class="well">Registration Form</h1>
    <div class="col-lg-12 well">
	    <div class="row">
				<form action="register.php" method="POST">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 form-group">
								<input type="text" placeholder="First Name" class="form-control" name="namefirst" required>
							</div>
							<div class="col-sm-6 form-group">
								<input type="text" placeholder="Last Name" class="form-control" name="namelast" required>
							</div>
						</div>					
						<div class="form-group">
							<input type="text" placeholder="Username" class="form-control" name="username" required>
						</div>	
  					<div class="form-group">
  						<input type="text" placeholder="Email Address" class="form-control" name="email" required>
  					</div>	
  					<div class="form-group">
  						<input type="password" placeholder="Password" class="form-control" name="password" required>
  					</div>	
  					<div class="form-group">
  						<input type="password" placeholder="Confirm Password" class="form-control" name="password_confirm" required>
  					</div>	
  					<input type="submit" class="btn btn-lg btn-primary" value="Submit">				
  				</div>
				</form> 
		  </div>
	  </div>
	</div>
<?php
include_once("postpage.php")
?>
