#! /bin/bash
# run from top-level project directory
docker run -d -h gear-site --name gear-site \
	-v $(PWD):/var/www/deepthought/ \
	-p 8001:443 \
	-p 8000:80 expressiveanalytics/dt-standard.php
docker exec -it gear-site composer install -d /var/www/deepthought/site/
