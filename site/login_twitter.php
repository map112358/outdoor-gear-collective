<?php
require_once dirname(__FILE__)."/site.inc.php";
$session = DTSession::sharedSession();
$dt_token = DTAPI::consumerTokenForAPI("gear");

//make sure we have a request token to authorize
$oauth_token = htmlentities($_REQUEST["oauth_token"]);
if(empty($oauth_token)){ // we need to have this first...
	header("Location: index.php");
	exit();
}


$tok=DTAPI::consumerTokenForAPI("twitter");
$consumer = new TwitterConsumer("twitter","",$tok);
$profile = $consumer->request("account/verify_credentials.json",array("fmt"=>"json"),"GET");

if(isset($profile,$profile["id"])){
	setcookie("login_method","twitter",time()+60*60*24*3000);
	$consumer = new DTProviderConsumer("gear","authentication.php",$dt_token);
	$consumer->request("authorize_twitter",array("tw_acc"=>$session["twitter_oauth_access_token"],"tw_sec"=>$session["twitter_oauth_access_secret"],"req_tok"=>$oauth_token,"twid"=>$profile["id"]));
	// we should be redirected to the DT verifier at this point
}
?>

<script>
history.back();
</script>

<?php
//require($postpage);
