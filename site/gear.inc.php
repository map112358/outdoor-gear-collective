<?php
include('vendor/autoload.php');

DTSettingsAPIs::initShared(__DIR__."/../local/apis.json");
DTSettingsStorage::initShared(__DIR__."/../local/storage.json");
DTSettingsConfig::initShared(__DIR__."/../local/config.json");

$db = DTSettingsStorage::connect("default");
$db->query("SET NAMES 'utf8'");

$params = new DTParams();

$salt = '$2a$07$ud07$udsadsadsaingfors';

$prepage = __DIR__."/prepage.php";
$prepageMain = __DIR__."/prepage_main.php";
$prepageNav = __DIR__."/prepage_nav.php";
$prepageAPI = __DIR__."/prepage_api.php";
$postpage = __DIR__."/postpage.php";
$postpageMain = __DIR__."/postpage_main.php";
$postpageFooter = __DIR__."/postpage_footer.php";