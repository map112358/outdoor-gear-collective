<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepageAPI);
	
	
  $page = $params->intParam('page');
  $callback = $params->stringParam('callback');
  $mConsumer = new DTProviderConsumer("gear","module.php",$dt_token);
  $mView = new VCList($params);
  $mView->requestItems($mConsumer);
  if(isset($params['pack_id'])) {//request for pack's gear
    $pack_id = $params['pack_id'];
    $consumer = new DTProviderConsumer("gear","pack.php",$dt_token);
    $pack=$consumer->request("by_id",array("id"=>$pack_id));
    $out = $mView->renderEach( function($obj) use ($page){
      global $pack;
    	$moduleView = new ModuleView();
    	return $moduleView->renderModalOverview($obj,$pack);
    });
    $f = array("fmt"=>"DTR","err"=>0,"obj"=>$out);
    echo $callback ."(". json_encode($f) .")";
  } else {
    $out = $mView->renderEach( function($obj) use ($page){
    	$moduleView = new ModuleView();
    	return $moduleView->renderOverview($obj,$page);
    });
    $f = array("fmt"=>"DTR","err"=>0,"obj"=>$out);
    echo $callback ."(". json_encode($f) .")";
  }