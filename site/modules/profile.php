<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	$module_id=$params->intParam("id");
	$consumer = new DTProviderConsumer("gear","module.php",$dt_token);
	$o=$consumer->request("by_id",array("id"=>$module_id));
	$is_owner = $user['id'] == $o['user']['id'];
  $gView = new GearListView(new DTParams());
  $gView->setItems($o['gear']);
?>

<div class="container main-container">
  <div class="table-sum">
  <h3><?=$o['name']?> <?=$o['category']['name']?> </h3>
  <h5><em>Contributed by <?=$o['user']['name_first']." ".$o['user']['name_last']?></em></h5>
  
  
  <?php if($gView->renderView($o['gear']) != "")
    echo "<h5>Weight: <span class='total'></span> oz</h5>";
  ?>
<?php
  echo $gView->renderView($o['gear']);
  echo "</div>";
  if($is_owner) {
    $search_consumer = DTSettingsConfig::baseURL("consumers/gear_consumer.php");
    echo <<<END
    <button data-toggle='modal' data-target='#addGearModal' class='btn btn-success' href='#'>Manage Gear</button>
    <div id='addGearModal' class='modal fade gear-module-modal'>
          <div class="modal-dialog">
  
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                <h4 class="modal-title">Gear Selector</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
              		<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchGearModuleModals('{$search_consumer}', '{$dt_token}', {$params['id']}, '#modal-gear-module-table', $(this).val().toLowerCase() )">
              	</div>
END;
        $dt_token = DTAPI::consumerTokenForAPI("gear");
        $consumer = new DTProviderConsumer("gear","gear.php",$dt_token);
        $gView = new GearListView($params);
        $gView->requestItems($consumer);
        echo $gView->renderModalView($o);
        echo <<<END
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
              </div>
            </div>
          </div>
        </div>
END;
  } else {
    
  }
  ?>
</div>

<script>
	// Each time the user scrolls in modal
  var win = $(".gear-module-modal");
  var page = 2;
	win.scroll(function() {
		if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
      console.log("get gearModule page");
			populateGearModal('<?=DTSettingsConfig::baseURL('gear/api.php')?>','<?=$dt_token?>',page,<?=$params['id']?>,'#modal-gear-module-table tbody',$('#search').val().toLowerCase())
      page += 1;
		}
	});
</script>

<?php
require($postpage);