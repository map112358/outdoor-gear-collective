<?php
	require_once dirname(__FILE__)."/../gear.inc.php";
	require($prepage);
	
	$mConsumer = new DTProviderConsumer("gear","module.php",$dt_token);
  $mView = new VCList($params);
  $mView->requestItems($mConsumer);

?>

<div class="container-fluid main-container">
  <div class="row">
    <div class="col-xs-12">
      <h3>Modules</h3>
      <div class="container-fluid">
        <div class="form-group">
        	<input id="search" type="text" placeholder="Search" class="form-control search" name="search" onkeyup="searchModels('<?=DTSettingsConfig::baseURL("consumers/module_consumer.php")?>', '<?=$dt_token?>', $(this).val().toLowerCase(), '.module-grid')">
        </div>
      </div>
      <div class="module-grid">
        <div class="grid">
          <?=$mView->renderEach( function($obj){
          	$moduleView = new ModuleView();
          	return $moduleView->renderOverview($obj);
          });?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
	// Each time the user scrolls in modal
  var page = 2;
  $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
      console.log("get module data");
    	populatePage('<?=DTSettingsConfig::baseURL("modules/api.php")?>','<?=$dt_token?>',page,'.grid',$('#search').val().toLowerCase())
      page += 1;
    }
  });
    
  $(document).ready(function() {
    resizeTextAreaEvent();
    fresh();
  });
</script>

<?php
	require($postpage);