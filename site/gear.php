<?php
require_once dirname(__FILE__)."/gear.inc.php";
require($prepage);

$gConsumer = new DTProviderConsumer("gear","gear.php",$dt_token);
$gView = new GearListView($params);
$gView->requestItems($gConsumer);
?>

<!-- 
  CLOTHES
-->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h3>Trending Gear</h3>
      <div class="grid">
        <?=$gView->renderView();?>
      </div>
    </div>
  </div>
  <div id="gear">   
    <?php
      /*
      if(isset($params['id'])) {
        $gear = new Gear($db->filter(array("id"=>$params['id'])));
        printGear($gear);
      } else if(isset($params['brand_id'])) {
        $brand = new Brand($db->filter(array("id"=>$params['brand_id'])));
        $gear = Gear::select($db->filter(array("brand_id"=>$params['brand_id'])));
        echo "<h3>".$brand['name']." Gear</h3>";
        printGearSearch();
        printGearTable($gear,array("category"=>0));
      } else if(isset($params['category_id'])) {
        $gear = Gear::select($db->filter(array("category_id"=>$params['category_id'])));
        printGearSearch();
        printGearTable($gear);//"picker"=>0));
      } else if(isset($params['module_id'])) {
        $gear = Gear::select($db->filter()->orderby("weight DESC"));
        $module = new Module($db->filter(array("id"=>$params['module_id'])));
        printGearSearch();
        printGearTable($gear,array("addToModule"=>$module,"owner"=>true,"category"=>0));
      } else if(isset($params['user_id'])) {
        $gear = Gear::select($db->filter(array("user_id"=>$params['user_id']))->orderby("weight DESC"));
        printGearSearch();
        printGearTable($gear,array("addToModule"=>$module,"owner"=>true,"category"=>0));
      } else {
        $gear = Gear::select($db->filter()->orderby("weight DESC"));
        printGearSearch();
        printCreateNewGear();
        printGearTable($gear,array("category"=>0));
        echo "<br><br><br>";
      }*/
    ?>
  </div>
</div>
<script>
  
  $("#search").keyup(function(){
      _this = this;
      // Show only matching TR, hide rest of them
      $.each($("#gear-table tbody tr"), function() {
          if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
             $(this).hide();
          else
             $(this).show();                
      });
  });
  
  $(document).ready(function() {
    sum = 0;
    $('#gear-table .weight').each(function(){
      sum += Number($(this).text())
    });
    $('#gear .total').each(function(){
      $(this).text(sum)
    })
    
} );
</script>


<?php
include_once("postpage.php")
?>