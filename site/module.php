<?php
require_once dirname(__FILE__)."/gear.inc.php";
require($prepage);

$mConsumer = new DTProviderConsumer("gear","module.php",$dt_token);
$mView = new VCList($params);
$mView->requestItems($mConsumer);
?>

<div class="container">
  
<?php
  /*$consumer = new DTProviderConsumer("gear","module.php",$dt_token);
  if (isset($params['id'])) {
    $mView = new ModuleView($params);
    
    $module = new Module($db->filter(array("id"=>$params['id'])));
    if($module['user']['id']==$user['id']) {
      printModule($module,array("owner"=>true,"carry"=>true,"module"=>true,"delGear"=>$module, "module_add_gear"=>true));
    } else {
      printModule($module,array("carry"=>true,"module"=>true,));
    }
  } else if(isset($params['module_category_id'])) {
    $modules = Module::select($db->filter(array('module_category_id'=>$params['module_category_id'],"user_id"=>$user['id']))->orderby("module_category_id ASC"));
    printModules($modules);
  } else if(isset($params['pack_id'])) {
    $modules = Module::select($db->filter(array("user_id"=>$user['id']))->orderby("module_category_id ASC"));
    $pack = new Pack($db->filter(array("id"=>$params['pack_id'])));
    printModules($modules,array('addToPack'=>true,'owner'=>true,'pack'=>$pack));
  } else if(isset($params['user_id'])) {
    $modules = Module::select($db->filter(array("user_id"=>$params['user_id']))->orderby("module_category_id ASC"));
    if($params['user_id'] == $user['id']) {
      printModules($modules,array("addModule"=>true,"delModule"=>true,"owner"=>true));
    } else {
      printModules($modules);
    }
  } else { */
?>
  <div class="row">
    <div class="col-xs-12">
      <h3>Trending Modules</h3>
      <div class="grid">
  <?=$mView->renderEach( function($obj){
  	$moduleView = new ModuleView();
  	return $moduleView->renderOverview($obj);
  });?>
      </div>
    </div>
  </div>
</div>
<script>

  $(document).ready(function() {
    fresh();
  });
</script>


<?php
include_once("postpage.php");
