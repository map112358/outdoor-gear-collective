<?php
include_once("prepage.php");  
?>

<script>
  addGearModule = function() {
    $.post("manageModules.php", {type: "create_link", module_id: $('#module_id').val(), gear_id: $('.gear_id option:selected').val()}).done(function(data){console.log(data)})
  }
  addModuleCategory = function() {
    $.post("manageModules.php", {type: "create_module", module_category_id: $('#module_category_id').val(), name: $('#module_name').val()}).done(function(data){console.log(data)})
  }
  addCategory = function() {
    $.post("manageModules.php", {type: "create_category", name: $('#category_name').val()}).done(function(data){console.log(data)})
  }
</script>
  
<h3>Modules</h3>

<h4>Create Category:</h4>
<input type="text" id="category_name">
<button onclick='addCategory()'>Save</button>

<h4>Create Module:</h4>
<input type="text" id="module_name">
<select id="module_category_id">
<?php
  $mcategories = ModuleCategory::select($db->filter(array()));
  foreach($mcategories AS $m) {
    echo "<option value='{$m['id']}'>{$m['name']}</option>";
  } 
?>
</select>
<button onclick='addModuleCategory()'>Save</button>    



<?php
  $moduleCategories = ModuleCategory::select($db->filter()->orderby("name ASC"));
?>
<h4>Select Existing Module</h4>
<form action="modules.php" method="get">
  <select name="id" class='module_id' onchange="this.form.submit()">
    <option></option>
<?php
  foreach($moduleCategories AS $mC) {
    foreach($mC['modules'] AS $m){
      echo "<option value='{$m['id']}'>{$mC['name']} {$m['name']}</option>";
    }
  }
?>
  </select>
</form>


<?php
if(isset($params['id'])) {
  $params1 = array("id"=>$params['id']);
  $module = new Module($db->filter($params1));
  $category = new ModuleCategory($db->filter(array("id"=>$module['module_category_id'])));
  ?>
  
  <h4>Add some gear to <?=$category['name'].": ".$module['name']?>.</h4>
  <table class="table table-hover">
    <tbody>
    <?php
    foreach($module['gear'] AS $g) {
      echo "<tr><td>{$g['brand']['name']} {$g['name']}</td><td>{$g['weight']}</td></tr>";
    }
    ?>
    </tbody>
  </table>
    <input type="hidden" id="module_id" value="<?=$params['id']?>"/>
    <select class='gear_id'>
    <?php
      $gear = Gear::select($db->filter()->orderby("weight DESC"));
      foreach($gear AS $g)
        echo "<option value='{$g['id']}'>{$g['brand']['name']} {$g['name']}</option>";
    ?>
    </select>
    <button name="add" onclick="addGearModule()">Add</button>
<?php
}
include_once("postpage.php")
?>