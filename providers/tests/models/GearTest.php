<?php
	require_once dirname(__FILE__)."/../../providers.inc.php";
	
	class GearTest extends DTTestCase {
		public function setup(){
			parent::setup();
			$this->production_store = DTSettingsStorage::connect("default");
		}
		
		// populate local SQLite database
		public function initSQL($sql = ""){
			return $sql.file_get_contents(__DIR__."/../init.sql");
		}
	}