<?php
    class Image extends DTModel{
	protected static $storage_table = 'image';
  protected static $has_many_manifest = array(
    "users"=>array("GCUser"),
    "usersName"=>array("UsersName")
  );
	public $name;
	public $file_ext;
	public $filename;
	
	function filename(){
  	return $this['id'].".".$this['file_ext'];
	}
}