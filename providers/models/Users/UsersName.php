<?php
class UsersName extends DTModel{
	protected static $storage_table = 'users';
  
  protected static $has_many_manifest = array(
    "friends"=>array("Follower"),
    "packs"=>array("Pack"),
    "modules"=>array("Module"),
    "gear"=>array("Gear")
  );
  
  protected static $has_a_manifest = array(
    "image"=>array("Image","image_id"),
    "image_header"=>array("Image","image_header_id")
  );
  
	public $name_first;
	public $name_last;
	public $name_trail;
	public $username;
	public $image;
	public $image_header;
}