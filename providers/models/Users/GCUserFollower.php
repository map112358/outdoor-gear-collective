<?php
	class GCUserFollower extends GCUser {
    protected static $has_many_manifest = array(
      "following"=>array("Followed","GCUser")
    );
    
    public $following;
		private $email;
		public $alias;
		public $name_first;
		public $name_last;
	}