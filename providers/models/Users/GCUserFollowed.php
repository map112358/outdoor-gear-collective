<?php
	class GCUserFollowed extends GCUser {
    protected static $has_many_manifest = array(
      "followed"=>array("Follower","GCUser")
    );
    
    public $followed;
		private $email;
		public $alias;
		public $name_first;
		public $name_last;
	}