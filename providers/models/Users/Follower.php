<?php
    
class Follower extends DTModel{
	protected static $storage_table = 'friend';
	
	protected static $has_a_manifest = array(
    "followed"=>array("GCUserFollowed","user_id_2"),
    "follower"=>array("GCUser","user_id_1")
  );
  
  public $user_id_1;
  public $user_id_2;
  public $follower;
  public $followed;
}