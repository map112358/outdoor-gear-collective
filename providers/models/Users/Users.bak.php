<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class UsersOld extends DTModel{
	protected static $storage_table = 'users';
  
  protected static $has_many_manifest = array(
    "modules"=>array("Module"),
    "packs"=>array("Pack"),
    "friends"=>array("Friend","UsersName"),
    "gear"=>array("Gear")
  );
  
  protected static $has_a_manifest = array(
    "image"=>array("Image","image_id"),
    "image_header"=>array("Image","image_header_id")
  );
  
	public $name_first;
	public $name_last;
	public $name_trail;
	public $username;
	public $gender_id;
	public $twitter_handle;
	public $packs;
	public $friends;
	public $image;
	public $image_header;
	public $gear;
}