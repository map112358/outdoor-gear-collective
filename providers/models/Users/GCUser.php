<?php
	class GCUser extends DTUser {
  			
		protected static $has_many_manifest = array(
      "modules"=>array("Module"),
      "packs"=>array("Pack"),
      "following"=>array("Follower","GCUserFollowed"),
      "followed"=>array("Followed","GCUserFollower")
      //"gear"=>array("Gear")
    );
    
    protected static $has_a_manifest = array(
      "image"=>array("Image","image_id"),
      "image_header"=>array("Image","image_header_id")
    );
    
    protected $email_encoded;
		protected $email_sha1;
		public $alias;
		public $name_first;
		public $name_last;
  	private $following;
  	private $followed;
  	public $image;
  	public $image_header;
  	public $image_id;
  	public $image_header_id;
  	public $about;
  	public $stats;
  	public $created_at;
		
    //public $image_header;
  	private $gear;
  	private $packs;
  	private $modules;
		private $email;
		
		function stats(){
  		$paks = Pack::select($this->db->filter(array("user_id"=>$this['id'])));
  		$ger = array();
  		foreach($paks AS $p) {
    		$ger = array_merge($p['gear'],$ger);
  		}
  		$mods = Module::select($this->db->filter(array("user_id"=>$this['id'])));
  		foreach($mods AS $m) {
    		$ger = array_merge($m['gear'],$ger);
  		}
  		$fol = Followed::select($this->db->filter(array("user_id_2"=>$this['id'])));
  		return array("follower_count"=>sizeof($fol),"gear_count"=>sizeof($ger),"module_count"=>sizeof($mods),"pack_count"=>sizeof($paks));
		}
		
		public function email(){
			return $this->email = $this->decode($this->email_encoded,"28330d45900b5ce6ed4990ed89b17801905b28e4");
		}
		
		public function setEmail($val){
			$this->email_sha1 = sha1($val);
			$this->email_encoded = $this->encode($val,"28330d45900b5ce6ed4990ed89b17801905b28e4");
		}
	}