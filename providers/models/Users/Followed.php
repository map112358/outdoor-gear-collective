<?php
    
class Followed extends DTModel{
	protected static $storage_table = 'friend';
	
	protected static $has_a_manifest = array(
    "follower"=>array("GCUserFollower","user_id_1"),
    "followed"=>array("GCUser","user_id_2")
  );
  
  public $user_id_1;
  public $user_id_2;
  public $follower;
  public $followed;
}