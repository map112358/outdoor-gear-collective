<?php
	class GCUserProfile extends GCUser {    
		private $email;
		public $alias;
		public $name_first;
		public $name_last;
  	public $packs;
  	public $modules;
  	public $following;
  	public $followed;
  	public $about;
  	public $gear;
  	
  	public function gear(){
  		$gear = array();
  		$mods = $this->modules;
  		foreach($mods as $m) {
    		$gear = array_merge($gear, $m['gear']);
  		}
  		$packs = $this->packs;
  		foreach($packs as $p) {
  		  foreach($p['modules'] as $m) {
    		  $gear = array_merge($gear, $m['gear']);
    		}
    		$gear = array_merge($gear, $p['gear']);
  		}
  		return array_unique($gear);
		}
	}