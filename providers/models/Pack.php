<?php
class Pack extends DTModel{
	protected static $storage_table = 'pack';
  
  protected static $has_many_manifest = array(
    "modules"=>array("PackModule","Module"),
    "gear"=>array("PackGear","Gear")
  );
  
  protected static $has_a_manifest = array(
    "user"=>array("GCUser","user_id")
  );
 
	public $name;
	public $modules;
	public $description;
	public $weight;
	public $weight_lb;
	public $user;
	public $gear;
	public $has_module;
	public $create_at;
	protected $deleted = 0;
	
	public function weight() {
  	$total = 0.0;
  	foreach($this['modules'] AS $m) {
    	$total += $m['weight'];
  	}
  	foreach($this['gear'] AS $g) {
    	$total += $g['weight'];
  	}
  	return $total;
	}
	
	public function weightLb() {
  	return number_format($this['weight']/16, 2, '.', '');
	}
}