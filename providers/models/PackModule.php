<?php
    class PackModule extends DTModel{
	protected static $storage_table = 'pack_module';
	
	protected static $has_a_manifest = array(
    "module"=>array("Module","module_id"),
    "pack"=>array("Pack","pack_id")
  );
}