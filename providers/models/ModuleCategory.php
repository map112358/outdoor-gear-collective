<?php    
class ModuleCategory extends DTModel{
	protected static $storage_table = 'module_category';
	
	protected static $has_many_manifest = array(
    "modules"=>array("Module"),
    "gear"=>array("Gear")
  );
  
	public $name;
	public $description;
	//public $modules;
}