<?php    
class Gear extends DTModel{
	protected static $storage_table = 'gear';
	
  protected static $has_a_manifest = array(
    "brand"=>array("Brand","brand_id"),
    "category"=>array("ModuleCategory","category_id"),
    "user"=>array("GCUser","user_id")
  );
  
  protected static $has_many_manifest = array(
    "modules"=>array("GearModule"),
    "packs"=>array("PackGear")
  );
  
	public $name;
	public $brand;
	public $weight;
	public $price;
	public $description;
	public $create_at;
	public $category;
	public $user;
	protected $deleted = 0;
}