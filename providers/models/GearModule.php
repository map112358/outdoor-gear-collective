<?php    
class GearModule extends DTModel{
	protected static $storage_table = 'gear_module';
	
	protected static $has_a_manifest = array(
    "gear"=>array("Gear","gear_id"),
    "module"=>array("Module","module_id")
  );
  
	public $gear;
	public $module;
}