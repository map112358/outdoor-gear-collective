<?php    
class Module extends DTModel{
	protected static $storage_table = 'module';
	
	protected static $has_a_manifest = array(
    "module_category"=>array("ModuleCategory","module_category_id"),
    "category"=>array("ModuleCategoryName","module_category_id"),
    "user"=>array("GCUser","user_id")
  );
  
  protected static $has_many_manifest = array(
    "gear"=>array("GearModule","Gear"),
    "packs"=>array("PackModule")
  );
  
	public $name;
	public $gear;
	public $category;
	public $description;
	public $user;
	public $weight;
	public $create_at;
	protected $deleted = 0;
	
	public function weight() {
  	$total = 0.0;
  	foreach($this['gear'] AS $g) {
    	$total += floatval($g['weight']);
  	}
  	return $total;
	}
}