<?php
class ModuleCategoryName extends DTModel{
	protected static $storage_table = 'module_category';
	
	protected static $has_many_manifest = array(
    "modules"=>array("Module")
  );
  
	public $name;
	public $description;
}