<?php
    class PackGear extends DTModel{
	protected static $storage_table = 'pack_gear';
	
	protected static $has_a_manifest = array(
    "gear"=>array("Gear","gear_id"),
    "pack"=>array("Pack","pack_id")
  );
}