<?php    
class Brand extends DTModel{
	protected static $storage_table = 'brand';

	protected static $has_many_manifest = array(
    "gear"=>array("Gear")
  );
  
	public $name;
	public $url;
}