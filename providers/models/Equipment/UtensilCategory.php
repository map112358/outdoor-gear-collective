<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class UtensilCategory extends DTModel{
	protected static $storage_table = 'utensil_category';
  
	public $name;
	public $description;
}