<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class SleepingBag extends DTModel{
	protected static $storage_table = 'sleeping_bag';
  
	public $degree_low_fahr;
	public $degree_high_fahr;
	public $foot_width_inch;
	public $shoulder_width_inch;
	public $length_inch;
	public $packed_radius_inch;
	public $packed_length_inch;
	public $sleeping_bag_type_id;
}