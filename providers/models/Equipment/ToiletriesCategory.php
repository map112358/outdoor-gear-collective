<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class ToiletriesCategory extends DTModel{
	protected static $storage_table = 'toiletries_category';
  
	public $name;
	public $description;
}