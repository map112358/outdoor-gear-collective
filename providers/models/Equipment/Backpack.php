<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class Backpack extends DTModel{
	protected static $storage_table = 'backpack';
  
	public $backpack_model_id;
	public $volume_liter;
	public $torso_low;
	public $torso_high;
	public $waist_low;
	public $waist_high;
	public $gender_id;
}