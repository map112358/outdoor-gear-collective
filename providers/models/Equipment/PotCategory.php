<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class PotCategory extends DTModel{
	protected static $storage_table = 'pot_category';
  
	public $name;
	public $description;
}