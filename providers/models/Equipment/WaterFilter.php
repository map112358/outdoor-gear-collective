<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class WaterFilter extends DTModel{
	protected static $storage_table = 'water_filter';
  
	public $volume_liter;
	public $water_filter_category_id;
	public $rate_liter_minute;
	public $lifespan_gallon;
}