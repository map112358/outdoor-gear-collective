<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class FirstAidCategory extends DTModel{
	protected static $storage_table = 'first_aid_category';
  
	public $name;
	public $description;
}