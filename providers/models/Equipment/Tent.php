<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class Tent extends DTModel{
	protected static $storage_table = 'tent';
  
	public $height_inch;
	public $floor_width_inch;
	public $floor_length_inch;
	public $number_seasons;
	public $number_doors;
	public $packed_radius_inch;
	public $packed_length_inch;
	public $tent_type_id;
}