<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class Stove extends DTModel{
	protected static $storage_table = 'stove';
  
	public $pack_length_inch;
	public $pack_width_inch;
	public $pack_height_inch;
	public $open_length_inch;
	public $open_width_inch;
	public $open_height_inch;
	public $stove_category_id;
}