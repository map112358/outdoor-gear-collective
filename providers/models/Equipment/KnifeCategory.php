<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class KnifeCategory extends DTModel{
	protected static $storage_table = 'knife_category';
  
	public $name;
	public $description;
}