<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class MultiToolCategory extends DTModel{
	protected static $storage_table = 'multi_tool_category';
  
	public $name;
	public $description;
}