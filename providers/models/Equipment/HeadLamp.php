<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class HeadLamp extends DTModel{
	protected static $storage_table = 'head_lamp';
  
	public $beam_distance_feet;
	public $brightness_lumen;
	public $battery_id;
}