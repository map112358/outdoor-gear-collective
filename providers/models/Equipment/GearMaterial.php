<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class GearMaterial extends DTModel{
	protected static $storage_table = 'gear_material';
  
	public $gear_id;
	public $material_id;
}