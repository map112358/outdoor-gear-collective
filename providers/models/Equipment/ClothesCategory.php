<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class ClothesCategory extends DTModel{
	protected static $storage_table = 'clothes_category';
  
	public $name;
	public $description;
}