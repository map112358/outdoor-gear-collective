<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class PackCoverCategory extends DTModel{
	protected static $storage_table = 'pack_cover_category';
  
	public $name;
	public $description;
}