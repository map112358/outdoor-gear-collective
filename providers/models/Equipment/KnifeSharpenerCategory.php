<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class KnifeSharpenerCategory extends DTModel{
	protected static $storage_table = 'knife_sharpener_category';
  
	public $name;
	public $description;
}