<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class StoveCategory extends DTModel{
	protected static $storage_table = 'stove_category';
  
	public $name;
	public $description;
}