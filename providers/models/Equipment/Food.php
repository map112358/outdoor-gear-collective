<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class Food extends DTModel{
	protected static $storage_table = 'food';
  
	public $calories;
	public $protein_gram;
	public $sugar_gram;
	public $servings;
	public $directions;
	public $vegetarian;
	public $gluten_free;
	public $food_category_id;
}