<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class Material extends DTModel{
	protected static $storage_table = 'material';
  
	public $name;
	public $description;
}