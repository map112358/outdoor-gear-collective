<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class WaterBottle extends DTModel{
	protected static $storage_table = 'water_bottle';
  
	public $volume_liter;
	public $bpa_free;
	public $water_bottle_category_id;
}