<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class Knife extends DTModel{
	protected static $storage_table = 'knife';
  
	public $blade_length;
	public $handle_length;
	public $knife_category_id;
}