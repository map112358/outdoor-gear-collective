<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class RopeCategory extends DTModel{
	protected static $storage_table = 'rope_category';
  
	public $name;
	public $description;
}