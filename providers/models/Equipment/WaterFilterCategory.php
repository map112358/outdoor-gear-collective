<?php
require_once dirname(__FILE__).'/../gear.inc.php';
    
class WaterFilterCategory extends DTModel{
	protected static $storage_table = 'water_filter_category';
  
	public $name;
	public $description;
}