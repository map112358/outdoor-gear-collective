<?php
	require_once dirname(__FILE__)."/providers.inc.php";
	$provider = new AdminUserProvider(new DTAdminVerifier(DTSettingsConfig::baseURL("index.php")));
	$provider->handleRequest();