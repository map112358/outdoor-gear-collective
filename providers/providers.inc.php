<?php
require_once dirname(__FILE__)."/vendor/autoload.php";

DTSettingsConfig::initShared(__DIR__."/../local/config.json");
DTSettingsStorage::initShared(__DIR__."/../local/storage.json");
DTSettingsAPIs::initShared(__DIR__."/../local/apis.json");

$db = DTSettingsStorage::defaultStore();