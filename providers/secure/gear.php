<?php
	require_once dirname(__FILE__)."/../providers.inc.php";
	$provider = new SecureGearProvider(new DTOAuthVerifier(DTSettingsConfig::baseURL("index.php")));
	$provider->handleRequest();