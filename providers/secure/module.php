<?php
	require_once dirname(__FILE__)."/../providers.inc.php";
	$provider = new SecureModuleProvider(new DTOAuthVerifier(DTSettingsConfig::baseURL("index.php")));
	$provider->handleRequest();