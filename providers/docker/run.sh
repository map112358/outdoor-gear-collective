#! /bin/bash
# run from top-level project directory
docker run -d -h gear-providers --name gear-providers \
	-v $(PWD):/var/www/deepthought/ \
	-p 8889:443 \
	-p 8888:80 expressiveanalytics/dt-standard.php
docker exec -it gear-providers composer install -d /var/www/deepthought/providers/
