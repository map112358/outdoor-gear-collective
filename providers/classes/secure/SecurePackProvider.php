<?php
	class SecurePackProvider extends PackProvider{
		public function actionAddPack(){
			$params = array();
			$params["user_id"] = $this->verifier->userID();
			$params["name"] = $this->params->stringParam("name");
			$params["description"] = $this->params->stringParam("description");
      $date = new DateTime();
      $data = $date->format('Y-m-d H:i:s');
    	$params['created_at'] = $date->getTimestamp();
			Pack::upsert($this->db->filter($params),$params);
		}
		
		public function actionDeletePack(){
    	$params = array();
    	$params['id'] = $this->params->intParam("pack_id");
			$p = new Pack($this->db->filter($params));
			$p->delete();
			$this->db->commit();
		}
	}