<?php
	class SecureUserProvider extends UserProvider{
		
		public function actionUpdate(){
			$params = $this->params->allParams();
			unset($params["is_admin"]);
			if(isset($params["file_ext"])) {
  			//insert image
  			$image = Image::upsert($this->db->filter(array("id"=>0)),array("file_ext"=>$params['file_ext']));
  			//DTLog::debug($params);
  			if(isset($params["profile_pic"])){
    			DTLog::debug("profile");
  			  $params['image_id'] = $image['id'];
        } else if(isset($params["banner_pic"])){
    			DTLog::debug("banner");
          $params['image_header_id'] = $image['id'];
        }
			}
			return GCUser::upsert($this->db->filter(array("id"=>$this->verifier->userID())),$params);
		}
		
		public function actionCurrentUser(){
			return GCUserFollower::byID($this->db,$this->verifier->userID());
		}
		
		public function actionFollow(){
  		$params1 = array(
  		  'user_id_1'=>$this->verifier->userID(),
  		  'user_id_2'=>$this->params->intParam('user_id')
  		);
			return Follower::upsert($this->db->filter($params1),$params1);
		}		
		
		public function actionUnfollow(){
  		$params1 = array(
  		  'user_id_1'=>$this->verifier->userID(),
  		  'user_id_2'=>$this->params->intParam('user_id')
  		);
  		$f = new Follower($this->db->filter($params1));
			$f->delete();
			$this->db->commit();
		}
	}