<?php
	class SecureModuleProvider extends ModuleProvider{
  	public function actionAddmodule(){
    	$params = array();
    	$params['user_id'] = $this->params->intParam("user_id");
    	$params['name'] = $this->params->stringParam("name");
    	$params['description'] = $this->params->stringParam("description");
    	$params['module_category_id'] = $this->params->intParam("module_category_id");
      $date = new DateTime();
    	$params['created_at'] = $date->format('Y-m-d H:i:s');
			return Module::upsert($this->db->filter(array("id"=>0)),$params);
		}
				
		public function actionDeleteModule(){
    	$params = array();
    	$params['id'] = $this->params->intParam("module_id");
			$p = new Module($this->db->filter($params));
			$p->delete();
			$this->db->commit();
		}
		
  	public function actionAddGearToModule(){
    	$params = array();
    	$params['module_id'] = $this->params->intParam("module_id");
    	$params['gear_id'] = $this->params->intParam("gear_id");
			return GearModule::upsert($this->db->filter($params),$params);
		}
		
  	public function actionRemoveGearFromModule(){
    	$params = array();
    	$params['module_id'] = $this->params->intParam("module_id");
    	$params['gear_id'] = $this->params->intParam("gear_id");
			$gm = new GearModule($this->db->filter($params));
			$gm->delete();
			$this->db->commit();
		}
				
		public function actionNewThisWeek(){
			$qb=$this->db->filter()->where("created_at > current_date - interval '7 days'");
			return array("count"=>Module::count($qb),"items"=>Module::select($qb,"Module.*"));
		}
	}