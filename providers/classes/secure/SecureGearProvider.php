<?php
	class SecureGearProvider extends GearProvider{
		public function actionAddgear(){
    	$params = array();
    	//DTLog::debug($this->params->allParams());
    	$params['user_id'] = 1;
    	$params['name'] = $this->params->stringParam("name");
    	$params['brand_id'] = $this->params->intParam("brand_id");
    	$params['weight'] = $this->params->doubleParam("weight");
    	$params['category_id'] = $this->params->intParam("category_id");
      $date = new DateTime();
    	$params['created_at'] = $date->format('Y-m-d H:i:s');
			//DTLog::debug($params);
			return Gear::upsert($this->db->filter(array("id"=>0)),$params);
		}
	}