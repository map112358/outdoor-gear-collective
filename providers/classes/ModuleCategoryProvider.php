<?php
	class ModuleCategoryProvider extends DTProvider{
		public function actionList($page=null,$count=null,$like=null){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",10);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			$qb=$this->db->filter(array());
			if($like!=""){
				$matches = "{$this->db->ilike} '%{$like}%'";
				$qb->where("name {$matches}");
			}
			
			$this->enforceOrder($qb);
			return array("count"=>ModuleCategory::count($qb),"items"=>ModuleCategory::select($qb->limit($limit),"ModuleCategory.*"));
		}
		
		public function actionByID(){
			try{
				return ModuleCategory::byID($this->db,$this->params->intParam("id"));
			}catch(Exception $e){}
			return null;
		}
		
		/** modify a query-builder to sort by a particular attribute
			
			@param $qb - the query builder to modify
			@param $order - the attribute to sort by (Encounters, Name, Diagnosis, Position)
			@param $desc - whether to sort in descending order */
		protected function enforceOrder($qb, $order="Position", $desc=true){
			$order = $this->params->stringParam("order");
			$desc = $this->params->boolParam("desc",false)?"DESC":"ASC";
			
			switch($order){
				case "County":
					$qb->orderBy("county {$desc}");
					break;
				default:
					$qb->orderBy("name {$desc}");
			}
		}
	}