<?php
	class GearProvider extends DTProvider{
		
		public function actionList($page=null,$count=null,$like=null){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",20);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			$qb=$this->db->filter(array("deleted"=>0));
			if($like!=""){
				$matches = "{$this->db->ilike} '%{$like}%'";
				$qb->where("name {$matches}");
			}
			
			$this->enforceOrder($qb);
			return array("count"=>Gear::count($qb),"items"=>Gear::select($qb->limit($limit),"Gear.*"));
		}		
		
		public function actionNewThisWeek(){
			$qb=$this->db->filter()->where("created_at > current_date - interval '7 days'");
			return array("count"=>Gear::count($qb),"items"=>Gear::select($qb,"Gear.*"));
		}
		
		public function actionSearch($page=null,$count=null,$like=null){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",20);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			$qb=$this->db->filter(array("deleted"=>0));
			if($like!=""){
				$matches = "{$this->db->ilike} '%{$like}%'";
				$qb->where("name {$matches}");
			}
			
			$this->enforceOrder($qb);
			return array("count"=>Gear::count($qb),"items"=>Gear::select($qb->limit($limit),"Gear.*"));
		}
		
		public function actionByID(){
			try{
				return Gear::byID($this->db,$this->params->intParam("id"));
			}catch(Exception $e){}
			return null;
		}
		
		/** modify a query-builder to sort by a particular attribute
			
			@param $qb - the query builder to modify
			@param $order - the attribute to sort by (Encounters, Name, Diagnosis, Position)
			@param $desc - whether to sort in descending order */
		protected function enforceOrder($qb, $order="Position", $desc=true){
			$order = $this->params->stringParam("order");
			$desc = $this->params->boolParam("desc",false)?"DESC":"ASC";
			
			switch($order){
				case "County":
					$qb->orderBy("county {$desc}");
					break;
				default:
					$qb->orderBy("name {$desc}");
			}
		}
	}