<?php
	class PackProvider extends DTProvider{
  	public function actionAddpack(){
    	$params = array();
    	$params['user_id'] = $this->params->intParam("user_id");
    	$params['name'] = $this->params->stringParam("name");
    	$params['description'] = $this->params->stringParam("description");
			//DTLog::debug($params);
      $date = new DateTime();
    	$params['created_at'] = $date->format('Y-m-d H:i:s');
			return Pack::upsert($this->db->filter(array("id"=>0)),$params);
		}
				
		public function actionNewThisWeek(){
			$qb=$this->db->filter()->where("created_at > current_date - interval '7 days'");
			return array("count"=>Pack::count($qb),"items"=>Pack::select($qb,"Pack.*"));
		}
		
  	public function actionAddGearToPack(){
    	$params = array();
    	$params['gear_id'] = $this->params->intParam("gear_id");
    	$params['pack_id'] = $this->params->intParam("pack_id");
			//DTLog::debug($params);
			return PackGear::upsert($this->db->filter($params),$params);
		}
		
  	public function actionRemoveGearFromPack(){
    	$params = array();
    	$params['gear_id'] = $this->params->intParam("gear_id");
    	$params['pack_id'] = $this->params->intParam("pack_id");
			//DTLog::debug($params);
			$pm = new PackGear($this->db->filter($params));
			$pm->delete();
			$this->db->commit();
		}
		
  	public function actionAddModuleToPack(){
    	$params = array();
    	$params['module_id'] = $this->params->intParam("module_id");
    	$params['pack_id'] = $this->params->intParam("pack_id");
			//DTLog::debug($params);
			return PackModule::upsert($this->db->filter($params),$params);
		}
		
  	public function actionRemoveModuleFromPack(){
    	$params = array();
    	$params['module_id'] = $this->params->intParam("module_id");
    	$params['pack_id'] = $this->params->intParam("pack_id");
			//DTLog::debug($params);
			$pm = new PackModule($this->db->filter($params));
			$pm->delete();
			$this->db->commit();
		}
		
		public function actionList($page=null,$count=null,$like=null){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",5);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			$qb=$this->db->filter(array("deleted"=>0));
			if($like!=""){
				$matches = "{$this->db->ilike} '%{$like}%'";
				$qb->where("name {$matches}");
			}
			
			$this->enforceOrder($qb);
			return array("count"=>Pack::count($qb),"items"=>Pack::select($qb->limit($limit),"Pack.*"));
		}
		
		public function actionSearch($page=null,$count=null,$like=null){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",5);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			$qb=$this->db->filter(array("deleted"=>0));
			if($like!=""){
				$matches = "{$this->db->ilike} '%{$like}%'";
				$qb->where("name {$matches}");
			}
			
			$this->enforceOrder($qb);
			return array("count"=>Pack::count($qb),"items"=>Pack::select($qb->limit($limit),"Pack.*"));
		}
		
		public function actionByID(){
			try{
				return Pack::byID($this->db,$this->params->intParam("id"));
			}catch(Exception $e){}
			return null;
		}
		
		/** modify a query-builder to sort by a particular attribute
			
			@param $qb - the query builder to modify
			@param $order - the attribute to sort by (Encounters, Name, Diagnosis, Position)
			@param $desc - whether to sort in descending order */
		protected function enforceOrder($qb, $order="Position", $desc=true){
			$order = $this->params->stringParam("order");
			$desc = $this->params->boolParam("desc",false)?"DESC":"ASC";
			
			switch($order){
				case "County":
					$qb->orderBy("county {$desc}");
					break;
				default:
					$qb->orderBy("name {$desc}");
			}
		}
	}