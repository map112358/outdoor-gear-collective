<?php
	/**
		@package Users
		@providers
		standard authentication handler for Gear Crossing users
	*/
	class GCAuthenticationProvider extends DTSSOAuthenticationProvider {
		public function castUser($qb){
			return new GCUser($qb);
		}
		
		/**
			generates a reset token for password updating
			@return string the password reset token
		*/
		public function actionPasswordResetToken(){
			$alias = $this->params->stringParam("alias");
			$user = new GCUser($this->db->filter(array("alias"=>$alias)));
			$token = new DTResetToken(array("alias"=>$user["alias"],"email"=>$user["email"])); //send back the email, not the alias
			$token->insert($this->db);
			return $token;
		}
		
		public function actionConnectSocial(){
			$social_user = new GCUser($this->db->filter(array("id"=>$this->verifier->userID())));
			$alias = $this->params->stringParam("alias");
			$password = $this->params->stringParam("password");
			$user = new GCUser($this->db->filter(array("alias"=>$alias)));
			if($user->verifyPassword($password)){ //hook us up
				$params = array();
				$params["facebook_id"] = $social_user["facebook_id"];
				$params["twitter_id"] = $social_user["twitter_id"];
				$params["google_plus_id"] = $social_user["google_plus_id"];
				GCUser::updateRows($this->db->filter(array("id"=>$user["id"])),$params); //this resets the password
				$session = DTSession::sharedSession();
				$session["pvd_user_id"] = $user["id"];
				GCUser::deleteRows($this->db->filter(array("id"=>$social_user["id"])));
			}
		}
	}