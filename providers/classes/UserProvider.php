<?php
	class UserProvider extends DTProvider{
		public function actionRegister(){
			$params = $this->params->allParams();
			DTLog::debug($params);
			return GCUser::upsert($this->db->filter(array("id"=>0)),$params);
		}
		
		/**
			check whether a given username could be a valid user alias
			@return true if alias does not exist, false otherwise
		*/
		public function actionValidUser(){
			$valid = false;
			$alias = $this->params->stringParam("alias");
			try{
				$existing = new GCUser($this->db->where("alias='{$alias}'"));
			}catch(Exception $e){$valid=true;} //this does not exist
			return  array("valid"=>$valid);
		}
		
		public function actionByID(){
  		$id = $this->params->intParam("id");
  		try{
    		return new GCUserProfile($this->db->filter(array("id"=>$id)));
  		}catch(Exception $e){}
  		return null;
  	}
		
		public function actionList(){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",10);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			$qb=$this->db->filter();
			
			return array("count"=>GCUser::count($qb),"items"=>GCUser::select($qb->limit($limit),"GCUser.*"));
		}		
		
		public function actionNewThisWeek(){
			$qb=$this->db->filter()->where("created_at > current_date - interval '7 days'");
			return array("count"=>GCUser::count($qb),"items"=>GCUser::select($qb,"GCUser.*"));
		}
		
		public function actionSearch($page=null,$count=null,$like=null){
			$page = $this->params->intParam("page",1)-1;
			$count = $this->params->intParam("count",10);
			$like = $this->params->stringParam("like");
			$limit = "{$count} OFFSET ".$page*$count;
			
			//DTLog::debug($this->params);
			$qb=$this->db->filter();
			if($like!=""){
				$matches = "{$this->db->ilike} '%{$like}%'";
				$qb->where("name_first {$matches} OR name_last {$matches}");
			}
			
			return array("count"=>GCUser::count($qb),"items"=>GCUser::select($qb->limit($limit),"GCUser.*"));
		}
	}