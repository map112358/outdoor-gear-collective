<?php
include('vendor/autoload.php');

DTSettingsStorage::initShared(__DIR__."/../local/storage.json");
DTSettingsConfig::initShared(__DIR__."/../local/config.json");

$db = DTSettingsStorage::connect("default");
$db->query("SET NAMES 'utf8'");

$params = new DTParams();