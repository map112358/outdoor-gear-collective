ignoreSet = set(["primary","foreign","key","public"])
pathToInc = "/../gear.inc.php"

def snakeToCamel(text):
  return text.replace("_"," ").title().replace(" ","")
  
def parseSQLAttributes(text):
  for a in text.split():
    if a.lower() in ignoreSet:
      return ""
  tokens = [a for a in text.split() if a.lower() not in ignoreSet]
  if len(tokens) > 1:
    return tokens[0]
  return ""
  
def parseSQLLine(text):
  fielddefs = text.split(",")
  fields = []
  for d in fielddefs:
    fields.append(parseSQLAttributes(d))
  return filter(len, fields)

def DTModelHeader(className,tableName):
  return """<?php
require_once dirname(__FILE__).'"""+pathToInc+"""';
    
class """+className+""" extends DTModel{
\tprotected static $storage_table = '"""+tableName+"""';
  
"""
  
state = 0
'''
  states:
  0: outside of CREATE TABLE statement
  1: in CREATE TABLE statement
'''

file = open("../../data/dump.sql","r")
outfile = open("../../models/README.TXT","w")
for f in file:
  if state is 1:
    attributes = parseSQLLine(f)
    for a in attributes:
      outfile.write("\tpublic $"+a+";\n")
  if "CREATE TABLE" in f:
    state = 1
    tableName = f.split("CREATE TABLE ")[1].split("(")[0].strip()
    className = snakeToCamel(tableName)
    outfile = open("../../models/"+className+".php","w")
    outfile.write(DTModelHeader(className,tableName))
    
  if ";" in f and state is 1:
    state = 0
    outfile.write("}")
