<?php
require_once dirname(__FILE__)."/scripts.inc.php";

$tables = $db->allTables();
foreach($tables as $t){
	$db->pullTable($t);
	echo $db->tableSQL($t,true,false);
	$db->purgeTable($t);
}