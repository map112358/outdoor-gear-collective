<?php
require_once dirname(__FILE__).'/../../gear.inc.php';
define("TRANSACTION_SIZE",1000);

$mapping = array(
"name"=>"name",
"weight"=>"weight",
"brand"=>"brand"
);

//load up the CSV from stdin
$file = "php://stdin";
$f = fopen($file,"r");
$headers = DTCSVStore::parseLine(fgets($f));
$headers = array_combine($headers, range(0,count($headers)-1));

$gear_cache = array();
$brand_cache = array();

$row_i = 0;
DTLog::debug("* ({$row_i})");
$db->begin();
while (($row=DTCSVStore::parseLineFromFile($f)) !== false){
	/* it's important that we iterate the mapping to preserve key order */
	$params = new DTParams(array_reduce(array_keys($mapping),function($out,$k) use ($mapping,$headers,$row){
		$i=$headers[$k]; $out[$mapping[$k]] = (isset($row[$i])?$row[$i]:null); return $out;},array()));
	$mapped = $params->allParams();
	
	try{
  	
  	if(!isset($brand_cache[$mapped["brand"]])){
    	$params = array("name"=>$mapped["brand"]);
			$brand = Brand::upsert($db->filter(array("name"=>array($db->ilike,$mapped["brand"]))),$params);
			$brand_cache[$mapped["brand"]] = $brand["id"];
		}
  	$brand_id = $brand_cache[$mapped["brand"]];
  	echo $brand_id."\n";
  	
  	$params = $mapped;
  	$params["brand_id"]=$brand_id;
  	unset($params["brand"]);
  	print_r($params);

		if(!isset($gear_cache[$mapped["name"]])){
			$gear = Gear::upsert($db->filter(array("name"=>array($db->ilike,$mapped["name"]))),$params);
			$gear_cache[$mapped["name"]] = $gear["id"];
		}
		$gear_id = $gear_cache[$mapped["name"]];
		
	}catch(Exception $e){ DTLog::debug("Failed to insert (%s):\n%s",$e->getMessage(),$mapped); }
	
	
	$row_i++;
	if($row_i%TRANSACTION_SIZE==0){
		//$db->rollback();
		$db->commit();
		$db->begin();
		DTLog::debug("* ({$row_i})");
	}
}
//$db->rollback();
$db->commit();